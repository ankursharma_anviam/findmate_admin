import { Routes, RouterModule } from '@angular/router';

import { ApprateComponent } from './appRate/appRate.component';

import { ModuleWithProviders } from '@angular/core';

export const appRateroutes: Routes = [
  {
    path: '', component: ApprateComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRateroutes);