import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { AppRateService } from '../appRate.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'policy',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appRate.component.scss'],
    templateUrl: './appRate.component.html',
    providers: [AppRateService]
})


export class ApprateComponent {
    ckeditorContent: any;
    pageForm: FormGroup;
    fileName: any;
    type = 1;
    data1: any;
    dataURI: any;
    public_id: any;
    cropperSettings: CropperSettings;
    croppedWidth: number;
    croppedHeight: number;
    cloudinaryImage: any;
    data = [];
    rowsOnPage= 10;
    p=1;
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }

    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    imageSrc: any;
    public pushData=[];
    constructor(private _appConfig: AppConfig, private router: Router,
        vcRef: ViewContainerRef, fb: FormBuilder, private _AppRateService: AppRateService) {

        this._AppRateService.getData().subscribe(result => {
            console.log(" result :  ", result.data)
            if (result.data) {
                this.data = result.data
            }
            this.data.forEach(e =>{
                this.pushData.push(e._id);
            })
        });

    }


    userDetailsModal(_id) {

        this._AppRateService
            .getProfileById(_id)
            .subscribe((user) => {

                if (user.data[0]) {
                    user = user.data[0];
                    $("#userDetails").modal("show");
                    console.log("user :: ", user);
                    this.userDetails.OtherPhotos.length = 0;
                    this.userDetails.OtherPhotos = [];
                    var years = moment().diff(user.dob, 'years');
                    this.userDetails.Name = user.firstName || "--";
                    this.userDetails.PhoneNo = user.contactNumber || "--";
                    this.userDetails.Email = user.email || "--";
                    this.userDetails.Gender = user.gender || "--";
                    this.userDetails.DateOfBirth = user.dob || 0;
                    this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
                    this.userDetails.Height = user.height + " cm , " + user.heightInFeet + " feet" || "--";
                    this.userDetails.ProfileVideo = user.profileVideo || "--";
                    this.userDetails.ProfilePhoto = user.profilePic || "https://help.github.com/assets/images/help/profile/identicon.png";
                    this.userDetails.OtherPhotos = user.otherImages || [];
                    this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
                    this.userDetails.Preferences = user.favoritePreferences || [];
                    this.userDetails.Age = years || "--";
                    this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
                        this.userDetails.RegisteredFrom = "--";
                    if (user.address && (user.address.city || user.address.country)) {
                        this.userDetails.RegisteredFrom = user.address.city + " ," || " ";
                        this.userDetails.RegisteredFrom += user.address.country  || " ";
                    }

                }
                error => console.log(error)
            });

    }

    getPage(p) {
        this._AppRateService
        .getAppRatePage(p , this.rowsOnPage).subscribe((data) => {
            {
               
                this.p = p;
                
            }
        });
        }
        ngOnInit(){
       
         }
         range = function(count){

            var ratings = []; 
          
            for (var i = 0; i < count; i++) { 
              ratings.push(i) 
            } 
          
            return ratings;
          }
}