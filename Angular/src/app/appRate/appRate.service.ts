import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class AppRateService {

    constructor(private http: Http, public _config: Configuration) {
    }


    getProfileByContectNumber(contectNumber) {
        let url = this._config.Server + 'profile/contactNumber/'+contectNumber;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getProfileByEmail(email) {
        let url = this._config.Server + 'profile/email/'+email;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    updatePolicyFile(data) {
        let url = this._config.Server + 'appRate';
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }

    getData() {
        let url = this._config.Server + 'appRate';
        return this.http.get(url,{ headers: this._config.headers }).map(res => res.json());
    }
    getProfileById(_id) {
        let url = this._config.Server + 'profile/'+_id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getAppRatePage(offset, limit){
        let Url = this._config.Server + 'subscription?planType=Active' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }   
}


// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
