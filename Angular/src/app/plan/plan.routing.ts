 import { Routes, RouterModule }  from '@angular/router';

import { PlanComponent } from './components/plan.component';
import { ModuleWithProviders } from '@angular/core';

export const planroutes: Routes = [ 
 {
       path: '',
       component: PlanComponent,
       data:{
         title: 'Plan'
       }
     
 }
];

export const routing: ModuleWithProviders = RouterModule.forChild(planroutes);