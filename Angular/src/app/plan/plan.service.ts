import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
declare var API_URL: string;
@Injectable()
export class PlanService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getplans() {
        let url = this._config.Server + "plan?planType=Active";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getDeleteplans() {
        let url = this._config.Server + "plan?planType=Deleted";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    addplans(value) {
        let url = this._config.Server + "plan";
        let body = JSON.stringify(value);
        // console.log("servicebody is===========>>>>>>", body);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    editPlan(value,) {
        let url = this._config.Server + "plan";
        let body = JSON.stringify(value);
        // console.log("servicebody of edit paln===========>>>>>>", body);
        return this.http.put(url, body,{ headers: this._config.headers }).map(res => res.json());
    }
    deleteplans(planId) {
        let url = this._config.Server + "plans" ;
        let body = {
            planId: planId
                }
        return this.http.put(url,body, { headers: this._config.headers }).map(res => res.json());
    }
    getplansById(_id) {
        let url = this._config.Server + "plan/" + _id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getUserBySearchtext(text) {
        let url = this._config.Server + "Plan/" + text;
        
        console.log( text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getPlanPage(offset, limit){
        let Url = this._config.Server + 'plan?planType=Active'+ '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }   
    getDeletePlanPage(offset, limit){
        let Url = this._config.Server + 'plan?planType=Deleted'+ '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }   
}



