import { Injectable } from '@angular/core';
import { RouterModule, Router }   from '@angular/router';

import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { Configuration } from '../../app.constant';
export class User {
    constructor(
        public username: string,
        public password: string) { }
}

@Injectable()
export class AuthService {

    constructor(private _router: Router,private _http: Http,public _config:Configuration) { }


    login(user: any) {

        let url = this._config.Server + "login";
        console.log("====>>>",this._config.headers)
        let body = {
            userName : user.username,
            password : user.password
        };
        return this._http.post(url, body, { headers: this._config.headers }).map(res => res.json()).
            map(res =>{
                let token = res.data.token
                    localStorage.setItem('adminToken', token);
                    this._config.setToken();
                return res;
            })
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    // checkCredentials() {
    //     if (localStorage.getItem("user") === null) {
    //         this._router.navigate(['login']);
    //     }
    // }
}
