import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';//m
import { contentHeaders } from './headers';//m
import { AuthService } from '../sharedServices/authService/auth.service';
@Component({
    selector: 'login',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./login.component.scss'],
    templateUrl: './login.component.html',
    providers: [AuthService]
})

export class LoginComponent {
    public router: Router;
    public form: FormGroup;
    public formForgotPas: FormGroup;
    public email: AbstractControl;
    public username: AbstractControl;
    public password: AbstractControl;
    public forgotEmail: AbstractControl;
    public token: string;
    public submitted: boolean = false;
    public error: boolean = false;
    public errorP: boolean = false;
    public errorI: boolean = false;
    public errorMessage: string = '';
    // public erro:string='';
    public forgotModal: boolean = false;

    constructor(router: Router, private fb: FormBuilder, public http: Http, public authService: AuthService) {
        console.log("sddsddsddssddsd")
        this.router = router;
        this.form = fb.group({
            // 'email': ['', Validators.required],
            'username': ['', Validators.required],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });
        this.formForgotPas = fb.group({
            'forgotEmail': ['', Validators.compose([Validators.required, emailValidator])]

        });

        //  this.email = this.form.controls['email'];
        this.username = this.form.controls['username'];
        this.password = this.form.controls['password'];
        this.forgotEmail = this.formForgotPas.controls['forgotEmail'];

        var currentUser = localStorage.getItem('adminToken');
        this.token = currentUser && currentUser;
    }

    public onSubmit(event, values: Object): void {
        this.submitted = true;
        if (this.form.valid) {
            event.preventDefault();
            this.authService.login(values)
                .subscribe((res) => {
                    console.log("res", res)
                    this.error = true;
                    this.errorMessage = res.message;
                    this.router.navigate(['pages/users']);

                    console.log("=======>", res.message)
                    // if (res.message == success) {
                    //     this.error = true;
                    //    this.errorMessage = res.message;
                    //     this.router.navigate(['pages/dashboard']);
                    // } else if (res.code == 3) {
                    //     this.error = true;
                    //     this.errorMessage = res.message;
                    //     setTimeout(() => this.error = false, 5000);
                    // } else {
                    //     this.errorP = true;
                    //     this.errorMessage = res.message;
                    //     setTimeout(() => this.errorP = false, 5000);
                    // }
                },
                    error => {
                        console.log("eorrrrrrrrrr", error, error.status);
                        this.errorI = true;
                        this.errorMessage = 'UserName or Password is not matched.';
                        setTimeout(() => this.errorI = false, 5000);
                    })
        }
    }

    clickmodal() {
        this.forgotModal = true;
    }
}


export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}