import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions,RequestMethod} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
 declare var API_URL: string;
@Injectable()
export class AppWalletService{
  
  constructor(private http : Http,public _config: Configuration){
  }
  getAppWallet(offset,limit) {
    let url = this._config.Server + 'walletApp?offset=' + offset + '&limit=' + limit;
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
}
getCoins() {
  let url = this._config.Server + "coin";
  return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
}
getSearchFromAppWallet(user) {
  let url = this._config.Server + 'walletAppByText?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo 
  return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
}
getSearchByText(user) {
  let url = this._config.Server + 'walletAppByText?text' + user;
  return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
}
getSearchByTxnType(user) {
  let url = this._config.Server + 'walletAppByText?txnType=' + user;
  return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
}
   

}

// this could also be a private method of the component class
function handleError (error: any) {
  // log error
  // could be something more sofisticated
  let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
  console.error(errorMsg);

  // throw an application level error
  return Observable.throw(errorMsg);
}
