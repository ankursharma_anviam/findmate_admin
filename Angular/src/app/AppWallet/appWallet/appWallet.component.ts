import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import { AppWalletService } from '../AppWallet.service';

import { ActivatedRoute } from '@angular/router';
import {
    AppState
} from "../../app.state";

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'appWallet',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appWallet.component.scss'],
    templateUrl: './appWallet.component.html',
    providers: [AppWalletService]
})

export class appWalletComponent implements OnInit {
    public CoinData: any[];
    public AppWallet: any[];
    public id: any;
    public deleteUser: any = [];
    ActiveDates = [];
    pushData = [];
    public rowsOnPage = 10;
    public p = 1;
    public totalusers;


    ActiveDatesSearch = {
        "dateFrom": 0, "dateTo": 0,
    };


    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef,
        private _state: AppState, private router: Router, private route: ActivatedRoute, private _appWalletService: AppWalletService,
        private _zone: NgZone) {

    }
    ngOnInit() {
        this.getAppWalletData(this.p);
        //this.getCoins();

    }
    getAppWalletData(p) {
        this._appWalletService.getAppWallet(p - 1, this.rowsOnPage).subscribe((data) => {
            this.AppWallet = data.data
            this.totalusers = data.totalCount;
            // console.log("result==============>", this.totalusers)
             this.p = p;
            console.log("AppWalletData==============>", this.AppWallet)

        })
    }
    getCoins() {
        this._appWalletService.getCoins().subscribe(
            result => {

                this.CoinData = result.data;

            }
        )
    }


    backToUsers() {
        this.router.navigate(['/pages/users']);
    }

    SearchByFromDate(date) {
        this.ActiveDatesSearch.dateFrom = new Date(date).getTime();
        this._appWalletService.getSearchFromAppWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.AppWallet = data.data;
            } else {
                this.AppWallet = [];
            }
        });
    }

    SearchByToDate(date) {
        this.ActiveDatesSearch.dateTo = new Date(date).getTime();
        this._appWalletService.getSearchFromAppWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.AppWallet = data.data;
            } else {
                this.AppWallet = [];
            }
        });
    }

    setTxnType(data) {
        this._appWalletService.getSearchByTxnType(data).subscribe((data) => {
            if (data.data) {
                this.AppWallet = data.data;
            } else {
                this.AppWallet = [];
            }
        });
    }

    SearchByText(data) {

        this._appWalletService.getSearchByText(data).subscribe((data) => {
            if (data.data) {
                this.AppWallet = data.data;
            } else {
                this.AppWallet = [];
            }
        });
    }



}











