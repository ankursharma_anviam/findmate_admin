import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { UsersService } from '../../users/users.service';
import { MatchesUserService } from '../MatchesUser.service';
// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'Matches',
    // encapsulation: ViewEncapsulation.None,
    styleUrls: ['./matches.component.scss'],
    templateUrl: './matches.components.html',
    providers: [UsersService, MatchesUserService]

})
export class Matches {
    ActiveDates = [];
    public prefData = [];
    public totalusers;
    public rowsOnPage = 10;
    public p = 1;
    videoplayer: any;
    ActiveDatesSearch = {


        "dateFrom": 0, "dateTo": 0,

    };
    loader = true;
    public data: any[];
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        about: "--",
        Gender: "Male",
        Height: "--",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }


    public chatDetails: any;
    constructor(private _userService: UsersService, private route: ActivatedRoute, private router: Router,
        private _MatchUserService: MatchesUserService, ) {
      
    }
    ngOnInit() {
        this.matches(this.p)
    }
    matches(p) {
        this._MatchUserService.getAllMatchUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            // console.log("data", data);
            if (data.data) {
                this.data = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("matched dataaaaaaaaaa", data)
            }
            error => console.log(error)
        });
    }
    toggleVideo(event: any) {
        this.videoplayer.nativeElement.play();
    }
    backToUsers() {
        this.router.navigate(['/pages/users']);
    }
    userDetailsModal(user) {
        this.prefData = []
        var Preferences = user.matchByMyPreferences
        Preferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.matchBydob, 'years');
        this.userDetails.Name = user.matchdBy || "--";
        this.userDetails.PhoneNo = user.matchByContactNumber || "--";
        this.userDetails.Email = user.matchByEmail || "--";
        this.userDetails.Gender = user.matchBygender || "--";
        this.userDetails.about = user.matchByabout || "--";
        this.userDetails.DateOfBirth = user.matchBydob || 0;
        this.userDetails.RegistrationDate = user.matchOn || "--";
        this.userDetails.Height = user.matchByheight + " cm , " + user.matchByheightInFeet + " feet" || "--";
        this.userDetails.ProfileVideo = user.matchByprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.ProfilePhoto = user.matchByprofilePic;
        this.userDetails.OtherPhotos = user.matchByOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if (user.matchBycity || user.matchBycountry) {
            this.userDetails.RegisteredFrom = user.matchBycity + " ," || " ";
            this.userDetails.RegisteredFrom += user.matchBycountry + " ," || " ";
        }

    }
    OpponentUserDetailsModal(user) {
        this.prefData = []
        var OpponentUserPreferences = user.opponentUserMyPreferences
        OpponentUserPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        console.log("-user.opponentUserprofileVideo-----",user.opponentUserprofileVideo)

        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.opponentUserdob, 'years');
        this.userDetails.Name = user.opponentUser || "--";
        this.userDetails.PhoneNo = user.opponentUserContactNumber || "--";
        this.userDetails.Email = user.opponentUserEmail || "--";
        this.userDetails.Gender = user.opponentUsergender || "--";
        this.userDetails.about = user.opponentUserByabout || "--";
        this.userDetails.DateOfBirth = user.opponentUserdob || 0;
        this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
        this.userDetails.Height = user.opponentUserheight + " cm , " + user.opponentUserheightInFeet + " feet" || "--";
        this.userDetails.ProfileVideo = user.opponentUserprofileVideo || "";
        this.userDetails.ProfilePhoto = user.opponentUserprofilePic;
        this.userDetails.OtherPhotos = user.opponentByOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if (user.opponentUsercity || user.opponentUsercountry) {
            this.userDetails.RegisteredFrom = user.opponentUsercity + " ," || " ";
            this.userDetails.RegisteredFrom += user.opponentUsercountry + " ," || " ";
            //   }
        }

    }
    public users: any[];
    id: number;


    gotoSearchText(term) {

        this._MatchUserService.getUserBytext(term).subscribe(
            (data) => {

                this.data = data.data;
                this.totalusers = data.data.length;
                console.log("yyyyyyyyyyy", data.data);
            });
    }

    FilterFromToSuperLikes() {
        this._MatchUserService.getSearchFromMatchesDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {

                this.data = data.data;
                this.totalusers = data.data.length;

            } else {
                this.data = [];
            }
        });
    }
    setMatchesFromDate(date) {
        this.ActiveDatesSearch.dateFrom = new Date(date).getTime();

        //this.ActiveDatesSearch.dateFrom= date;
        //this.ActiveDatesSearch.dateTo= date;
        this._MatchUserService.getSearchFromMatchesDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.data = data.data;
                this.totalusers = data.data.length;

            } else {
                this.data = [];
            }
        });
    }

    setMatchesToDate(date) {
        this.ActiveDatesSearch.dateTo = new Date(date).getTime();
        // this.ActiveDatesSearch.dateTo = date
        this._MatchUserService.getSearchFromMatchesDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.data = data.data;
                this.totalusers = data.data.length;

            } else {
                this.data = [];
            }
        });
    }

}