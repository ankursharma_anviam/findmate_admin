//================= importing all the required modules ==============
import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//============ importing the router module ==============
import { RouterModule } from '@angular/router';

//importing ng2-pagination
import { Ng2PaginationModule } from 'ng2-pagination';

//============ importing ProfileLikesComponent ================
import { ProfileLikesComponent } from './components/profileLikes/profileLikes.component';
import { ProfileViewsComponent } from './components/profileViews/profileViews.component';



@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    Ng2PaginationModule,

  ],
  declarations: [
    ProfileLikesComponent,
    ProfileViewsComponent
  ],

  providers: [],
  exports: [
  ]


})
//============= exporting customer module ==========================
export class ProfilesModule {

}