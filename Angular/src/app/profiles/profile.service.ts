import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';
import { Configuration } from '../app.constant';
@Injectable()
export class ProfilesService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getLikes(): Observable<any[]> {
        return this.http
            .get(API_URL + '/profiles/likes', { headers: contentHeaders })
            .map((response: Response) => <any[]>response.json())
            .catch(handleError);
    }
    getSuperLikes(seachControl, flag) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });
        let body = { searchP: seachControl, flag: flag };
        let Url = API_URL + '/profiles/superLikes';
        return this.http.post(Url, body, options)
            .map(res => res.json())
    }
    getUnLikes(): Observable<any[]> {
        return this.http
            .get(API_URL + '/profiles/unlikes', { headers: contentHeaders })
            .map((response: Response) => <any[]>response.json())
            .catch(handleError);
    }

    getViews(): Observable<any[]> {
        return this.http
            .get(API_URL + '/profiles/views', { headers: contentHeaders })
            .map((response: Response) => <any[]>response.json())
            .catch(handleError);
    }
    getProfileByFbId(id) {
        let url = this._config.Server + "userDetail/" + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    searchlikes(seachControl) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });
        let body = { searchP: seachControl };
        let Url = API_URL + '/profiles/likes/search';
        return this.http.post(Url, body, options)
            .map(res => res.json())
    }
    searchUnlikes(seachControl) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });
        let body = { searchP: seachControl };
        let Url = API_URL + '/profiles/unlikes/search';
        return this.http.post(Url, body, options)
            .map(res => res.json())
    }


    searchViews(seachControl) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });
        let body = { searchP: seachControl };
        let Url = API_URL + '/profiles/views/search';
        return this.http.post(Url, body, options)
            .map(res => res.json())
    }


}


// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
