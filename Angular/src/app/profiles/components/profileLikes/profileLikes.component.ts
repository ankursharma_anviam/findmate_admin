import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, OnInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router } from '@angular/router';

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';


//=================== importing ProfilesService   =============
import { ProfilesService } from '../../profile.service';


@Component({
    selector: 'profileLikes',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./profileLikes.component.scss'],
    templateUrl: './profileLikes.component.html',
    providers: [ProfilesService]
})

export class ProfileLikesComponent implements OnInit, AfterViewInit {
    public loader: boolean = false;

    public profLikes: any[];
    public profUnLikes: any[];

    private profileLikes: any[];
    private profSuperLikes: any[];
    private profExist: any;
    private pageTitle: number = 1;
    private noLikesdata: boolean = false;
    private noSuperLikesdata: boolean = false;
    private noUnLikesdata: boolean = false;

    //search
    searchLikes = new FormControl();
    searchUnLikes = new FormControl();
    searchSuperLikes = new FormControl();
    public config: any;
    public configFn: any;

    constructor(private _profileService: ProfilesService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
    }

    ngOnInit() {

        this.getLikes()

        this.searchLikes.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this._profileService.searchlikes(seachControl))
            .subscribe((data: any) => {
                //  console.log(data)
                if (data.errCode == 201) {
                    this.noLikesdata = false
                    this.profLikes = data.response.data
                }
                else if (data.errNum == 400) {
                    this._profileService.getLikes()
                        .subscribe((res: any) => {
                            if (data) {
                                this.noLikesdata = false
                                this.profLikes = res.response.data;
                            }
                            else {
                                this.noLikesdata = true
                            }
                        });
                }
                else if (data.errNum == 135) {
                    this.profLikes = [];
                    this.noLikesdata = true
                }
            });

        this.searchUnLikes.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this._profileService.searchUnlikes(seachControl))
            .subscribe((data: any) => {
                //  console.log(data)
                if (data.errCode == 201) {
                    this.noUnLikesdata = false
                    this.profUnLikes = data.response.data
                }
                else if (data.errNum == 400) {
                    this._profileService.getUnLikes()
                        .subscribe((res: any) => {
                            if (data) {
                                this.noUnLikesdata = false
                                this.profUnLikes = res.response.data;
                            }
                            else {
                                this.noUnLikesdata = true
                            }
                        });
                }
                else if (data.errNum == 135) {
                    this.profUnLikes = [];
                    this.noUnLikesdata = true
                }
            });
        this.searchSuperLikes.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this._profileService.getSuperLikes(seachControl, 1))
            .subscribe((data: any) => {
                //  console.log(data)
                if (data.errCode == 201) {
                    this.noSuperLikesdata = false
                    this.profSuperLikes = data.response.data
                }
                else {
                    this.profSuperLikes = [];
                    this.noSuperLikesdata = true
                }
            });



    }



    refresh() {
        this.getLikes()
        //   this.searchControl = new FormControl();
    }


    ngAfterViewInit() {

    }

    getLikes() {
        this._profileService
            .getLikes()
            .subscribe((data: any) => {
                this.noLikesdata = false
                //  console.log(data)
                if (data.errCode == 201) {
                    this.profLikes = data.response.data

                } else {
                    this.noLikesdata = true
                }
            })
    }

    getUnLikes() {
        this._profileService
            .getUnLikes()
            .subscribe((data: any) => {
                this.noUnLikesdata = false
                if (data.errCode == 201) {
                    this.profUnLikes = data.response.data

                } else {
                    this.noUnLikesdata = true
                }
            })

    }
    getSuperLikes() {
        this._profileService
            .getSuperLikes({}, 0)
            .subscribe((data: any) => {
                this.noSuperLikesdata = false
                if (data.errCode == 201) {
                    this.profSuperLikes = data.response.data

                } else {
                    this.noSuperLikesdata = true
                }
            })

    }

    LikeFunc() {
        this.profUnLikes = []
        this.profSuperLikes = []
        this.getLikes();
        this.pageTitle = 1;

    }
    superLikeFunc() {
        this.profLikes = []
        this.profUnLikes = []
        //  this.profSuperLikes=[]
        this.getSuperLikes();
        this.pageTitle = 2;

    }
    unLikeFunc() {
        this.profLikes = []
        this.profSuperLikes = []
        this.getUnLikes();
        this.pageTitle = 0;
    }
    profile(fbid) {
        this.loader = true

        this._profileService
            .getProfileByFbId(fbid, 1)
            .subscribe((data) => {
                if (data.errCode == 201) {
                    console.log(data)
                    this.loader = false
                    this.profExist = data.response.data;


                }
                this.loader = false
                error => console.log(error)
            });



    }
    profilePic(value) {
        if (value == null || value == "" || value == "string") {
            return "assets/img/app/noimage.png"

        }
        return value;
    }
    checkDate(value) {
        if (value == null || value == "" || value == "string" || !value) {
            return "00-00-0000 14:10:19"

        }
        return value;
    }
}