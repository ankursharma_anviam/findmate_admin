 import { Routes, RouterModule }  from '@angular/router';

import { subscriptionComponent } from './components/subscription.component';
import { ModuleWithProviders } from '@angular/core';

export const subscriptionroutes: Routes = [ 
 {
       path: '',
       component: subscriptionComponent,
       data:{
         title: 'Subscription'
       }
     
 }
];

export const routing: ModuleWithProviders = RouterModule.forChild(subscriptionroutes);