import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';

import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import {
    subscriptionService
} from '../subscription.service';


import {
    AppState
} from "../../app.state";

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'subscription',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./subscription.component.scss'],
    templateUrl: './subscription.component.html',
    providers: [subscriptionService]
})

export class subscriptionComponent implements OnInit {


    public rowsOnPage = 10;
    public rowsOnPage1 = 10;
    public p = 1;
    public totalusers;
    private SubcriptionData: any[];
    public deleteSubcriptionData: any[];
    public addplan: FormGroup;
    public editPlan: FormGroup;
    oldpaln: any;
    oldcurrencySymbol: any;
    oldcost: any;
    oldcurrencyCode: any;
    olddurationInDays: any;
    oldlikeCount: any;
    oldrewindCount: any;
    oldstatusCode: any;
    oldsuperlikeCount: any;
    oldhideDistance: any;
    oldhideAge: any;
    public users: any[];
    public deleteUser = [];
    public id: any;
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }



    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef, private _subscriptionService: subscriptionService,
        private _state: AppState, private router: Router,
        private _zone: NgZone) {



    }

    ngOnInit() {
        this.getSubcriptionDetails(this.p);
        // this.getExpiredSubcriptionDetails(this.p1)

    }
    getSubcriptionDetails(p) {
       
        this._subscriptionService.getSubscription(p - 1, this.rowsOnPage).subscribe(
            result => {
                this.SubcriptionData = result.data;
                this.totalusers = result.totalCount;
               // console.log("result==============>", this.totalusers)
                this.p = p;
               // console.log("getSubcriptionDetails==============>", this.SubcriptionData)

            }
        )
    }
    getExpiredSubcriptionDetails(p) {
      
        this._subscriptionService.getExpiredSubscription(p - 1, this.rowsOnPage).subscribe(
            result => {
                this.deleteSubcriptionData = result.data;
                this.totalusers = result.totalCount;
              //  console.log("result==============>", this.totalexusers)

                this.p = p;

              // console.log("getExpiredSubcriptionDetails==============>", this.deleteSubcriptionData)

            }
        )
    }

    gotoSearchSubcription(term) {
        this._subscriptionService.getUserBySearchtext(term).subscribe(
            (data) => {
                if (data.data) {
                    this.SubcriptionData = data.data;
                } else {
                    this.SubcriptionData = [];
                }
                console.log("yyyyyyyyyyy", data.data);
            });
        if (term == "") {
            this.getSubcriptionDetails(this.p);
        }
    }

    gotoSearchExpiredSubcription(term) {
        this._subscriptionService.getUserBySearchtextofexpire(term).subscribe(
            (data) => {
                if (data.data) {
                    this.deleteSubcriptionData = data.data;

                    console.log("yyyyyyyyyyy", data.data);
                } else {
                    this.deleteSubcriptionData = [];
                }
            });
        if (term == "") {
            this.getExpiredSubcriptionDetails(this.p);
        }
    }



}











