//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing ProfileLikesComponent,ProfileViewsComponent Components ================
import { ProfileLikesComponent } from './components/profileLikes/profileLikes.component';
import { ProfileViewsComponent } from './components/profileViews/profileViews.component';
 
export const profLikesRoutes: Routes = [ 
         {
       path: 'likes',
       component: ProfileLikesComponent,
         data:{
         title: 'Likes'
       }
      },
      {
       path: 'views',
       component: ProfileViewsComponent,
         data:{
         title: 'Views'
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(profLikesRoutes); // not required


