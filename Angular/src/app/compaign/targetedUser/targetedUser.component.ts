import { Component, ViewEncapsulation, Input, ViewChild, Inject, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppConfig } from "../../app.config";
import {
    Router
} from '@angular/router';

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//import { IMyDpOptions } from 'mydatepicker';
import { FileUploaderModule } from "ng4-file-upload";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray
} from '@angular/forms';
//import { SearchPipe } from "../../theme/pipes/search/search.pipe";
import {
    FileUploader, FileSelectDirective
} from 'ng2-file-upload';
//import {videoPlayer} from '@angular-video';
import {
    AgmCoreModule,
    MapsAPILoader
} from 'angular2-google-maps/core';
import {
    ImageCropperComponent,
    CropperSettings,
    Bounds
} from 'ng2-img-cropper';

//importing service


import { Subject } from 'rxjs/Subject'

import {
    AppState
} from "../../app.state";

// import "../../../../node_modules/jquery/dist/jquery-ui.css";

import "../../../../node_modules/jquery/dist/jquery-1.9.1.js";
import "../../../../node_modules/jquery/dist/jquery-ui.js";
import { validateConfig } from '@angular/router/src/config';
import { resolve } from 'path';
import { Pipe, PipeTransform } from '@angular/core';
import { MyDatePickerModule } from 'mydatepicker';
import { compaignService } from '../compaign.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../users/users.service';



declare var $: any;





@Component({
    selector: 'targetedUserComponent',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./targetedUser.component.scss'],
    templateUrl: './targetedUser.component.html',
    providers: [compaignService, UsersService]
})



export class targetedUserComponent {
    public no = 1;
    public users: any[];
    loader = true;
    public data: any[];
    public rowsOnPage = 10;
    public p = 1;
    detail: any;




    private userDetailForm: FormGroup;



    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }


    constructor(private _loader: MapsAPILoader, private route: ActivatedRoute, private router: Router, private _compaignService: compaignService,
        private _usersService: UsersService, private formBuilder: FormBuilder) {

    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            let _id = params['id'];
            console.log("id - ", _id)

            this._compaignService
                .getTargetedUsers(_id)
                .subscribe((data) => {
                    console.log("gettargetedUserById data", data.data, this.data);
                    if (data.data[0]) {
                        this.data = data.data;
                    }
                    error => console.log(error)
                    this.loader = false;
                });
        });




        this.userDetailForm = this.formBuilder.group({ // nottt

            Name: ['', Validators.required],

            DateOfBirth: ['', Validators.compose([Validators.required])],
            Email: ['', Validators.compose([Validators.required])],
            //   coinBal: ['', Validators.compose([numValidator])],
            Gender: ['', Validators.required],
            // about: [''],
            Age: [''],
            Height: [''],
            lat: ['', Validators.required],
            lng: ['', Validators.required],
            city: ['', Validators.required],
            country: ['', Validators.required],
            fullpath: [''],
            PhoneNo: ['', Validators.required],
            values: this.formBuilder.array([]),
            valuesUser: this.formBuilder.array([]),
            OtherImages: this.formBuilder.array([]),
            "slideval": [''],
            "singleVal": [''],
            dstTyp: [''],
            lmtTyp: [''],
            RegisteredFrom: ['', Validators.required]

        })


    }




    backToUsers() {
        this.router.navigate(['/pages/compaign']);
    }
    userDetailsModal(users) {
        this._compaignService
            .getTargetedUsers(users._id)
            .subscribe((user) => {
                var detail = user.data
                detail.forEach(e => {
                    if (users.usersId == e.usersId) {
                        this.userDetails.ProfilePhoto = e.profilePic;
                        this.userDetails.Name = e.firstName || "--";
                        this.userDetails.PhoneNo = e.contactNumber || "--";
                        this.userDetails.Email = e.email || "--";
                        this.userDetails.Gender = e.gender || "--";
                        if (e.city || e.country) {
                            this.userDetails.RegisteredFrom = e.city + " ," || " ";
                            this.userDetails.RegisteredFrom += e.country || " ";
                        }
                        $("#userDetails").modal("show");
                    }
                });
                error => console.log(error)
            });
    }
    getPage(p) {
        this._compaignService
            .getCompaignPagination(p - 1, this.rowsOnPage).subscribe((data) => {
                {

                    this.p = p;

                }
            });
    }

}

