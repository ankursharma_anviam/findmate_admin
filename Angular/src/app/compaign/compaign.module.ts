import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import {Ng2PaginationModule} from 'ng2-pagination'; 
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyDatePickerModule } from 'mydatepicker';
// import { DirectivesModule } from "../theme/directives/directives.module";

import {compaignUserPageComponent} from './compaignUserPage/compaignUserPage.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { newCompaignsComponent} from './component/newCompaigns.component';
import {targetedUserComponent} from './targetedUser/targetedUser.component';
import { viewsCompaignComponent} from './viewsCompaign/viewsCompaign.component';
import { clickedCompaignComponent} from './clickedCompaign/clickedCompaign.component';
//  export const routes = [
//    { path: '', redirectTo: 'newCompaign', pathMatch: 'full' },
//    { path: 'newCompaign', component: newCompaignComponent, data: { breadcrumb: 'newCompaign' } }
// ];
import { routing } from './compaign.routing';
@NgModule({
  imports: [
    Ng2PaginationModule,
    MyDatePickerModule,
    CommonModule,
    ChartsModule,
    // DirectivesModule,
    RouterModule,
 
    CKEditorModule,
   routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  declarations: [
    compaignUserPageComponent,
    targetedUserComponent,
    newCompaignsComponent,
    viewsCompaignComponent,
    clickedCompaignComponent

  ],


})
export class compaignModule {

}