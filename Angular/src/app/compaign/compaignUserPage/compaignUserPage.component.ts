import { Component, ViewEncapsulation, Input, ViewChild, Inject, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppConfig } from "../../app.config";
import {
    Router
} from '@angular/router';

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//import { IMyDpOptions } from 'mydatepicker';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray
} from '@angular/forms';
//import { SearchPipe } from "../../theme/pipes/search/search.pipe";
import {
    FileUploader, FileSelectDirective
} from 'ng2-file-upload';
//import {videoPlayer} from '@angular-video';
import {
    AgmCoreModule,
    MapsAPILoader
} from 'angular2-google-maps/core';
import {
    ImageCropperComponent,
    CropperSettings,
    Bounds
} from 'ng2-img-cropper';

//importing service


import { Subject } from 'rxjs/Subject'

import {
    AppState
} from "../../app.state";

// import "../../../../node_modules/jquery/dist/jquery-ui.css";

import "../../../../node_modules/jquery/dist/jquery-1.9.1.js";
import "../../../../node_modules/jquery/dist/jquery-ui.js";
import { validateConfig } from '@angular/router/src/config';
import { resolve } from 'path';
import { Pipe, PipeTransform } from '@angular/core';
import { MyDatePickerModule } from 'mydatepicker';
import { compaignService} from '../compaign.service';
import { UsersService} from '../../users/users.service';
declare var $: any;
declare var swal: any;

@Component({
    selector: 'compaignUserPageComponent',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./compaignUserPage.component.scss'],
    templateUrl: './compaignUserPage.component.html',
    providers: [compaignService, UsersService]
})



export class compaignUserPageComponent  {
    private loader: boolean = false;
    public users=[];
    public imageData=[];
    private userDetailForm:FormGroup;
    public deleteUser=[];
    public myglobalUrl:any;
    
    private ids: any[] = [];
public pMsg :boolean= true;
public rowsOnPage = 5;

public p = 1;

    constructor(private _loader: MapsAPILoader, private router: Router,private _compaignService: compaignService,private _usersService: UsersService,  private formBuilder: FormBuilder){

    }
    
    ngOnInit() {
      this.getCompaign()
      this.getPage(this.p);
      this.myglobalUrl ='https://xxxxxxxxx.xom/datum_2.0-admin/no-image-icon.png'
    }
  
    getCompaign(){
    this._compaignService.getAllcompaign().subscribe(
        data => {
            this.users = data.data;

              console.log("from comapign construster...",this.users)
        });
    }
   newCompaigns() {
        this.loader = true;
        this.router.navigate(['/pages/compaign/component']);
    }
    targetedUsers(_id) {
        this.loader = true;
        this.router.navigate(['/pages/targetedUser/' + _id ]);
    }
   views(_id) {
        this.loader = true;
         this.router.navigate(['/pages/viewsCompaign/' + _id]);
    }
   clicked(_id) {
        this.loader = true;
         this.router.navigate(['/pages/clickedCompaign/' + _id]);
    }
    getPage(p) {
        this._compaignService
            .getCompaignPagination(p, this.rowsOnPage).subscribe((data) => {
                {
                  
                    this.p = p;
                   
                }
            });
    }

    Deleteuser(id) {
        this._compaignService.deleteUser(id).subscribe((data) =>{
            swal("Success!","Campaing Deleted  Successfully!", "success");
            this.getCompaign(); 

        })
       
        
    }
    public selectedData;
    onSelect(val){
        console.log(val);
        this.selectedData = this.users.filter(x => x.value == val)
      }

      closeModal(){
         //this.users=[]
        jQuery('#sendPush').modal('toggle');

    }

    CompaignVisitors(id){
        // this.users=[]
        jQuery('#sendPush').modal('show')
        this.pMsg=true
        this.users.forEach(e => {
            if(e._id == id){
                this.imageData.push({
                     
                    "campainImageUrl":e.campainImageUrl,
                    "campainTitle":e.campainTitle,
                    "message":e.message

                })

            }
            
        });
    }
    
   
}

