 import { Routes, RouterModule }  from '@angular/router';

import { friendShipComponent } from './components/friendShip.component';
import { ModuleWithProviders } from '@angular/core';

export const friendShiproutes: Routes = [ 
 {
       path: '',
       component: friendShipComponent,
       data:{
         title: 'friendship'
       }
     
 }
];

export const routing: ModuleWithProviders = RouterModule.forChild(friendShiproutes);