import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
declare var API_URL: string;
@Injectable()
export class friendShipService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getSubscription(offset, limit, status) {
        let url = this._config.Server + 'friendShip?status=' + status + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    updateStatus(id,status) {
        let data = {
            id: id,
            status: status,
        }
        let url = this._config.Server + "friendShip";
        return this.http.put(url, JSON.stringify(data), { headers: this._config.headers })
            .map(res => res.json());
    }



}



