import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { routing } from './friendShip.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
//============ importing UsersComponent  ================
import {friendShipComponent } from './components/friendShip.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule
  ],
  declarations: [
    friendShipComponent,
   ]
})
 export class friendShipModule { }
 
                                     