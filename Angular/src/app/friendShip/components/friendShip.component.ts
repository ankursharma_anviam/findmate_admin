import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';

import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import {
    friendShipService
} from '../friendShip.service';


import {
    AppState
} from "../../app.state";

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'friendShip',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./friendShip.component.scss'],
    templateUrl: './friendShip.component.html',
    providers: [friendShipService]
})

export class friendShipComponent implements OnInit {


    public rowsOnPage = 10;
    public rowsOnPage1 = 10;
    public p = 1;
    public totalusers;
    private SubcriptionData: any[];
    public deleteSubcriptionData: any[];
    public addplan: FormGroup;
    public editPlan: FormGroup;
    oldpaln: any;
    oldcurrencySymbol: any;
    oldcost: any;
    oldcurrencyCode: any;
    olddurationInDays: any;
    oldlikeCount: any;
    oldrewindCount: any;
    oldstatusCode: any;
    oldsuperlikeCount: any;
    oldhideDistance: any;
    oldhideAge: any;
    public users: any[];
    public deleteUser = [];
    public id: any;
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }



    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef, private _subscriptionService: friendShipService,
        private _state: AppState, private router: Router,
        private _zone: NgZone) {



    }

    ngOnInit() {
        this.getSubcriptionDetails(this.p);
        // this.getExpiredSubcriptionDetails(this.p1)

    }
    getSubcriptionDetails(p) {

        this._subscriptionService.getSubscription(p - 1, this.rowsOnPage, 1).subscribe(
            result => {
                this.SubcriptionData = result.data;
                this.totalusers = result.totalCount;
                this.p = p;

            }
        )
    }
    getExpiredSubcriptionDetails(p) {

        this._subscriptionService.getSubscription(p - 1, this.rowsOnPage, 2).subscribe(
            result => {
                this.deleteSubcriptionData = result.data;
                this.totalusers = result.totalCount;


                this.p = p;



            }
        )
    }

    getUnfriendedDetails(p) {
        this._subscriptionService.getSubscription(p - 1, this.rowsOnPage, 3).subscribe(
            (result) => {
                this.deleteSubcriptionData = result.data;
                this.totalusers = result.totalCount;


                this.p = p;
            })
    }

    updateStatus(data, status) {

        console.log("=================data", data)
        this._subscriptionService.updateStatus(data, status).subscribe(
            (data) => {
                swal("Success!", "Success!", "success");
                this.ngOnInit()

            });

    }



}











