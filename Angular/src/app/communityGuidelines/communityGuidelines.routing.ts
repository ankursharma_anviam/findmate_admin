import { Routes, RouterModule } from '@angular/router';

import { communityGuidelinesComponent } from './communityGuidelines/communityGuidelines.component';

import { ModuleWithProviders } from '@angular/core';

export const communityguidelinesroutes: Routes = [
  {
    path: '', component: communityGuidelinesComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(communityguidelinesroutes);