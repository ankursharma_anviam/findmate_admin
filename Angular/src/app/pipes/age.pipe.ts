import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'agePipe'
})
export class AgePipe implements PipeTransform {
    
  transform(value: any): any {
    if(!value){
      return 0
    }
   var date1=new Date(value);
      var ageDifMs = Date.now() - date1.getTime();
     var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970)
 }
}