import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keyValuePipe'
})
export class keyValuePipe implements PipeTransform {
  static forRoot() {
    return {
        ngModule: keyValuePipe,
        providers: [],
    };
 }
 
    //transform(value, args:string[]) : any {
    //let keyValuePipe = [];
      // for (let key in value) {
      // for (let [key, value] of Object.entries(value)) {

      // keyValuePipe.push(key);
      //  keyValuePipe.push({key: key, value: value[key]});
      // console.log("keyvalueeeeeeeeeee",keyValuePipe)
        
    // }
    // return keyValuePipe;


    transform(value, args:string[]) : any {
      let keys = [];
      for (let key in value) {
        keys.push({key: key, value: value[key]});
      }
      return keys;
    }
  }


  //  transform(objArr) {
  //   console.log('obj array looks like this: ', objArr);
  //   let mappingObject = {};
  //   objArr.forEach((obj) => {
  //     for (let key in obj) {
  //       let value = obj[key];
  //       mappingObject[key] = value;
  //     }
  //   })
  //   return mappingObject;
  // }
  // }
 
    
     
