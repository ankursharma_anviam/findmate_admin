import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userTypePipe'
})
export class UserType implements PipeTransform {

  transform(value: any, args?: any[]): any {

    //console.log(value.planDetails)

    if (value.planDetails) {
      return "Paid"
    }
    return "Free"

  }
}