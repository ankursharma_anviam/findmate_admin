import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prfpic'
})
export class PrfPic implements PipeTransform {

  transform(value: any, args?: any[]): any {

  // console.log(value)
if(value==null || value== "" || value=="string" ){
    return "assets/img/app/noimage.png"

   }
       return value;
}
}