import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
declare var $: any;
@Component({
    selector: 'deviceLogs',
    // encapsulation: ViewEncapsulation.None,
    styleUrls: ['./deviceLogs.component.scss'],
    templateUrl: './deviceLogs.components.html',
    providers: [UsersService]

})
export class DeviceLogs {
    public p = 1;
    public rowsOnPage = 10
    public id
    loader = true;
    data = {};

    public chatDetails: any;
    constructor(private _userService: UsersService, private route: ActivatedRoute,
        private router: Router, private _usersService: UsersService) {
        this.route.params.subscribe(params => {
            let _id = params['id'];
            console.log("id - ", _id)
            this.data = {};
            this._usersService
                .getDeviceLogById(_id)
                .subscribe((data) => {
                    console.log("data", data);
                    if (data.data) {
                        this.data = data.data;
                    }
                    error => console.log(error)
                });
        });
    }

    backToUsers() {
        this.router.navigate(['/pages/users']);
    }
    ngOnInit() {
    }
}