import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './components/users.component';
import { RecentVisitorsComponents } from './recentVisitors/recentVisitors.components';
import { ModuleWithProviders } from '@angular/core';
import {superLikes} from "./superLikes/superLikes.components";
// import {superLikesPageComponents} from "./superLikesPage/superLikesPage.components";
// import { BannedUserPageComponents} from './bannedUsersPage/bannedUserPage.components';
// import { deactivatePageComponents} from './deactivatePage/deactivatePage.components';
// import { ReportUserPageComponents} from './reportedUsersPage/reportedUserPage.components';
//import{ UsersComponent} from './components/users.component';
import { BlockUserDetailComponents} from './BlockUserDetail/BlockUserDetail.components'

export const usersroutes: Routes = [
  {
    path: '', component: UsersComponent, data: { title: '' },
   
  },
  //{
//     path: ':bannedUsersPage',
//       component:BannedUserPageComponents ,
//     data:{
//       title: 'Ban User'
//     }
  
// },
// {
//   path: ':deactivatePage',
//     component:deactivatePageComponents ,
//   data:{
//     title: 'Deactivate User'
//   }

// },
// {
//   path: ':reportedUsersPage',
//     component:ReportUserPageComponents ,
//   data:{
//     title: 'Reported User Page'
//   }

// },
{
  path: ':BlockUserDetail',
    component:BlockUserDetailComponents ,
  data:{
    title: 'Block User Page Detail'
  }

},
// {
//   path: ':component',
//     component:deactivatePageComponents ,
//   data:{
//     title: 'New Compaign'
//   }

// }
];

export const routing: ModuleWithProviders = RouterModule.forChild(usersroutes);