import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';
import { Configuration } from '../app.constant';
@Injectable()
export class PreferenceService {

    constructor(private http: Http, public _config: Configuration) {
    }
    getAll() {
        let url = this._config.Server + 'preferences';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getDiscoveryPrefByStatus() {
        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: contentHeaders
        });
        let Url = API_URL + '/preferences/discovery';
        return this.http.get(Url, options)
            .map(res => res.json());
    }
    getUserPrefByStatus() {
        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: contentHeaders
        });
        let Url = API_URL + '/preferences/user';
        return this.http.get(Url, options)
            .map(res => res.json());
    }
    addPrefrences(pref,dropValue,unitVal,optionval) {
        let body = {
            PreferenceTitle:pref,
            TypeOfPreference:dropValue,
            OptionsValue:optionval || "",
            optionsUnits:unitVal,

        }
       // console.log("service body of add preference",body);
        let url = this._config.Server + "preference";
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
        
    }

    deletePref(prefId) {
        let url = this._config.Server + "preference/"+prefId;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }
    getPrefById(id) {
        let options = new RequestOptions({
            method: RequestMethod.Get,
            headers: contentHeaders
        });
        let Url = API_URL + "/preferences/";
        let UrlWithId = Url + id;
        return this.http.get(UrlWithId, options)
            .map(res => res.json());
    }

    updatePref(value) {
        let options = new RequestOptions({
            method: RequestMethod.Put,
            headers: contentHeaders
        });
        let body = value;
        // body["id"]=id;
        console.log(body);
        let Url = API_URL + '/preferences';
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    search(seachControl) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });
        let body = { searchP: seachControl };
        let Url = API_URL + '/preferences/search';
        return this.http.post(Url, body, options)
            .map(res => res.json())
    }

    incrPriority(pref) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });

        let body = { _id: pref._id, priority: pref.Priority };
        let Url = API_URL + '/preferences/priorityInc';
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }

    decrPriority(pref) {
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: contentHeaders
        });

        let body = { _id: pref._id, priority: pref.Priority };
        let Url = API_URL + '/preferences/priorityDec';
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }




}


// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
