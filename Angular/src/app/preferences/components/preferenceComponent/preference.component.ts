import { Component, ViewEncapsulation, ViewContainerRef, AfterContentInit, OnInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router } from '@angular/router';

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
//=================== importing PreferenceService   =============
import { PreferenceService } from '../../preference.service';
declare var $: any;

declare var swal: any;


@Component({
    selector: 'preference',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./preference.component.scss'],
    templateUrl: './preference.component.html',
    providers: [PreferenceService]
})

export class PreferenceGeneratorComponent implements OnInit {
    private TOP: any;
    private TOPUSER: any;
    public loader: boolean = false;
    public preferences: any[];
    public prefsettings: any;
    private createPrefForm: FormGroup;
    private editPrefForm: FormGroup;
    private nodata: boolean = false;
    private hasError: boolean = false;
    private hasErrorE: boolean = false;
    private ValueExist: boolean = false;
    public DelId: string;
    //search
    searchControl = new FormControl();
    private disable = false;
    private pageNum: number = 0;
    public tickbox: any;
    public config: any;
    public configFn: any;
    public counter: any;
    public length: any;
    public options: any;
    public name: string;
    public optionval = [];
    public unitval = [];

    constructor(private _prefService: PreferenceService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.counter = 0;
    }

    ngOnInit() {
        this.initAddPrefsettingForm()
        this.getAll();
        this.searchControl.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this._prefService.search(seachControl))
            .subscribe(data => {
                console.log(data)
                this.preferences = data.response.data

                if (data.errCode == 201) {
                    this.nodata = false
                    this.preferences = data.response.data

                    this.disable = true
                }
                else if (data.errNum == 400) {

                    this.pageNum = 0;
                    this._prefService.getAll()
                        .subscribe((res) => {
                            if (data) {
                                this.nodata = false
                                this.preferences = res.allData;

                            }
                            else {
                                this.nodata = true
                            }
                        });
                }
                else if (data.errNum == 135) {
                    this.preferences = [];

                    this.nodata = true
                }
            });

        this.initEditSPrefsettingForm()
    }


    initEditSPrefsettingForm() {
        this.editPrefForm = this.fb.group({
            "_id": [''],
            "PreferenceTitle": ['', Validators.compose([
                Validators.required])],
            "TypeOfPreference": [{ value: '', disabled: true }, Validators.compose([
                Validators.required])],
            // "TypeOfPreference_user": [{ value: '', disabled: true }, Validators.compose([
            //     Validators.required])],
            // "Start": ['', Validators.compose([
            //     Validators.required])],
            // "Start_user": ['', Validators.compose([
            //     Validators.required])],
            // "End": ['', Validators.compose([
            //     Validators.required])],
            // "End_user": ['', Validators.compose([
            //     Validators.required])],
            "Unit": ['', Validators.compose([
                Validators.required])],
            // "Unit_user": ['', Validators.compose([
            //     Validators.required])],
            // "Text": ['', Validators.compose([
            //     Validators.required])],
            // "Text_user": ['', Validators.compose([
            //     Validators.required])],
            "OptionsValue": this.fb.array([
            ]),
            // "OptionsValue_user": this.fb.array([
            // ]),
        })
    }
    initAddPrefsettingForm() {
        this.createPrefForm = this.fb.group({
            "PreferenceTitle": ['', Validators.compose([
                Validators.required])],
            "TypeOfPreference": [this.TOP, Validators.compose([
                Validators.required])],
            // "TypeOfPreference_user": ['', Validators.compose([
            //     Validators.required])],
            // "Start": ['', Validators.compose([
            //     Validators.required])],
            // "Start_user": ['', Validators.compose([
            //     Validators.required])],
            // "End": ['', Validators.compose([
            //     Validators.required])],
            // "Unit": ['', Validators.compose([
            //     Validators.required])],
            // "End_user": ['', Validators.compose([
            //     Validators.required])],
            // "Unit_user": ['', Validators.compose([
            //     Validators.required])],
            // "Text": ['', Validators.compose([
            //     Validators.required])],
            // "Text_user": ['', Validators.compose([
            //     Validators.required])],
            "ActiveStatus": [1, Validators.compose([
                Validators.required])],
            "OptionsValue": this.fb.array([

            ]),
            // "OptionsValue_user": this.fb.array([
            // ])
        })
    }
    initOptions() {
        // return this.fb.control('')
        return this.fb.control('', Validators.required)
    }
    addOptions() {
        const control = <FormArray>this.createPrefForm.controls['OptionsValue'];
        control.push(this.initOptions());
    }

    deleteOptions(i: number) {
        const control = <FormArray>this.createPrefForm.controls['OptionsValue'];
        control.removeAt(i)  // like  spice in js
    }

    addOptionsUser() {
        const control = <FormArray>this.createPrefForm.controls['OptionsValue_user'];
        control.push(this.initOptions());
    }
    deleteOptionsUser(i: number) {
        const control = <FormArray>this.createPrefForm.controls['OptionsValue_user'];
        control.removeAt(i)  // like  spice in js
    }
    refresh() {
        this.getAll()
        this.searchControl = new FormControl();
    }
    check(e) {
        const control = <FormArray>this.createPrefForm.controls['OptionsValue'];
        let length = control.length
        console.log(control.controls)
        console.log("control length " + length)
        while (length--) {
            control.removeAt(length)
            console.log(control.controls)
            console.log("index " + length)
        }

        console.log(this.createPrefForm.value)
    }
    getAll(): void {
        this._prefService
            .getAll()
            .subscribe((data) => {
                {
                    if (data) {
                        this.nodata = false
                        this.preferences = data.data.Mypreference[0]
                        console.log("===========>",this.preferences)

                    } else {
                        this.nodata = true
                    } console.log(data)

                }

            })
    }

    ShowHideDiv() {
        var drpoDownValue = document.getElementById("drpoDownValue");
        var dvforRadio = document.getElementById("dvforRadio");
        dvforRadio.style.display = drpoDownValue.value == "1" ? "block" : "none";

        var dvforslider = document.getElementById("dvforslider");
        var dvforUnit = document.getElementById("dvforUnit");
        if (drpoDownValue.value == "3" || drpoDownValue.value == "4") {
            dvforslider.style.display = "block",
                dvforUnit.style.display = "block"
        }
        else {
            dvforslider.style.display = "none",
                dvforUnit.style.display = "none"
        }
        //dvforslider.style.display = drpoDownValue.value == "s" ? "block" : "none";

    }
    prefrenceModel() {
        jQuery('#addPrefrence').on('hidden.bs.modal', function () {
            jQuery(this).find("input,textarea,select").val('').end();
        });
        $('#addPrefrence').modal('show');
    }
    public count = [];
    countText = 0;
    addValue(i) {
        this.countText = this.countText + 1;
        this.count.push(this.countText);
    }

    addpref(pref, min, max) {
        var dropValue = document.getElementById("drpoDownValue").value;
        var unitVals = document.getElementById("unitValue").value;
        if (dropValue == 4 || dropValue == 3) {
            this.optionval.push(parseInt(min), parseInt(max));
        }

        if (dropValue == 1) {
            this.count.forEach(e => {
                this.optionval.push(jQuery('.radioVal' + e).val())
            });
        }

        this._prefService
            .addPrefrences(pref, dropValue, unitVals, this.optionval)
            .subscribe((data) => {
                {
                    if (data) {
                        this.optionval = [];
                        swal("Success!", "Prefrence Aded!", "success");
                        this.getAll();
                        $('#addPrefrence').modal('hide');

                        console.log("preference Inserted succesfully")
                    } else {
                        console.log("error")
                    }

                }

            })




    }
    updatePref({ valid }: { valid: boolean }, j, id) {
        if (this.editPrefForm.value.PreferenceTitle && this.editPrefForm.value.Text || this.editPrefForm.value.PreferenceTitle && this.editPrefForm.value.Start && this.editPrefForm.value.End && this.editPrefForm.value.Unit
            || this.editPrefForm.value.PreferenceTitle && this.editPrefForm.value.OptionsValue.length >= 1
        ) {
            valid = true;
        } else {
            valid = false;
        }

        if (valid) {
            this.loader = true;
            //this.tickbox=j,
            console.log(id),
                console.log('update')
            console.log(this.editPrefForm.value);
            this._prefService
                .updatePref(this.editPrefForm.value)
                .subscribe((data) => {
                    this.loader = false;
                    this.tickbox = null;
                    this.hasErrorE = false;
                    //console.log(data)
                    if (data.errCode == 201) {
                        // this.preferences = null
                        this._prefService.getAll()
                            .subscribe(data => {
                                if (data) {
                                    this.nodata = false
                                    this.preferences = data.data
                                    this.hasError = false
                                }
                                else {
                                    this.nodata = true
                                }
                            });

                    } else { alert(data.message); }


                })
        } else {
            this.hasErrorE = true;

        }


    }
    // deleteButton(prefId) {
    //     this.DelId = prefId;
    // }
    deleteButton(prefId) {
        this._prefService.deletePref(prefId)
            .subscribe((data) => {
                if (data) {
                    swal("Success!", "Prefrence Deleted!", "success");
                    this.getAll();
                }



            })

    }
    incPriority(pref) {
        console.log(pref)
        this._prefService.incrPriority(pref)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 201) {
                    this._prefService.getAll()
                        .subscribe(data => {
                            this.preferences = data.allData
                        });
                }
            })
    }


    decPriority(pref) {
        console.log(pref)
        this._prefService.decrPriority(pref)
            .subscribe((data) => {
                console.log(data)
                if (data.errCode == 201) {
                    this._prefService.getAll()
                        .subscribe(data => {
                            this.preferences = data.allData
                        });
                }
            })
    }

    editButton(j, id) {
        this.initEditSPrefsettingForm()
        console.log(id),
            //console.log('editdiv')
            this.tickbox = j

        this._prefService.getPrefById(id)
            .subscribe((data) => {
                // console.log(data)
                this.prefsettings = data.response.data;
                this.initEditPrefsettingForm()
                let optValues: any[] = this.prefsettings.OptionsValue
                if (optValues && optValues.length > 0) {
                    console.log(optValues)
                    this.prefsettings.OptionsValue.forEach((data) => {
                        const control = <FormArray>this.editPrefForm.controls['OptionsValue']
                        control.push(this.createEditPrefOptions(data));
                    })
                }
                let optValues_user: any[] = this.prefsettings.OptionsValue_user
                if (optValues_user && optValues_user.length > 0) {
                    console.log(optValues_user)
                    this.prefsettings.OptionsValue_user.forEach((data) => {
                        const control_user = <FormArray>this.editPrefForm.controls['OptionsValue_user']
                        control_user.push(this.createEditPrefOptions(data));
                    })
                }
                this.ValueExist = true;

            })


    }
    initEditPrefsettingForm() {
        this.editPrefForm = this.fb.group({
            "_id": this.prefsettings._id,
            "PreferenceTitle": [this.prefsettings.PreferenceTitle, Validators.compose([
                Validators.required])],
            "TypeOfPreference": [{ value: this.prefsettings.TypeOfPreference, disabled: true },
            Validators.compose([
                Validators.required])],
            // "TypeOfPreference_user": [{ value: this.prefsettings.TypeOfPreference_user, disabled: true },
            // Validators.compose([
            //     Validators.required])],
            // "Start": [this.prefsettings.Start, Validators.compose([
            //     Validators.required])],
            // "End": [this.prefsettings.End, Validators.compose([
            //     Validators.required])],
            "Unit": [this.prefsettings.Unit, Validators.compose([
                Validators.required])],
            // "Start_user": [this.prefsettings.Start_user, Validators.compose([
            //     Validators.required])],
            // "End_user": [this.prefsettings.End_user, Validators.compose([
            //     Validators.required])],
            // "Unit_user": [this.prefsettings.Unit_user, Validators.compose([
            //     Validators.required])],
            // "Text": [this.prefsettings.OptionsValue, Validators.compose([
            //     Validators.required])],
            // "Text_user": [this.prefsettings.OptionsValue_user, Validators.compose([
            //     Validators.required])],
            "OptionsValue": this.fb.array([
            ]),
            // "OptionsValue_user": this.fb.array([
            // ])
        })
    }
    createEditPrefOptions(val) {
        return this.fb.control(val, Validators.required)
    }
    addOptionsE() {
        var val = ''
        const control = <FormArray>this.editPrefForm.controls['OptionsValue'];
        control.push(this.createEditPrefOptions(val));
    }
    deleteOptionsE(i: number) {
        const control = <FormArray>this.editPrefForm.controls['OptionsValue'];
        control.removeAt(i)
    }
    addOptionsE_user() {
        var val = ''
        const control = <FormArray>this.editPrefForm.controls['OptionsValue_user'];
        control.push(this.createEditPrefOptions(val));
    }
    deleteOptionsE_user(i: number) {
        const control = <FormArray>this.editPrefForm.controls['OptionsValue_user'];
        control.removeAt(i)
    }
    cancelEdit() {
        this.tickbox = null;
    }
    disablePref(prefStat) {
        if (prefStat == 1) {
            return true
        } else {
            return false
        }
    }


}



