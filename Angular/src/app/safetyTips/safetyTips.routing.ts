import { Routes, RouterModule } from '@angular/router';

import { safetyTipsComponent } from './safetyTips/safetyTips.component';

import { ModuleWithProviders } from '@angular/core';

export const safetytipsroutes: Routes = [
  {
    path: '', component: safetyTipsComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(safetytipsroutes);