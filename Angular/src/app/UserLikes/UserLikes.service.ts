import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
declare var $q: any;

declare var API_URL: string;
@Injectable()
export class UserLikesService {

    constructor(private http: Http, public _config: Configuration) {


    }


    getUserByLikestext(text) {
        let Url = this._config.Server + "userActivityFilters?userType=1" + "&text=" + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByDisLikestext(text) {
        let Url = this._config.Server + "userActivityFilters?userType=2" + "&text=" + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserBySuperLikestext(text) {
        let Url = this._config.Server + "userActivityFilters?userType=3" + "&text=" + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByBoosttext(text) {
        let Url = this._config.Server + "userBoost?&text=" + text;
        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteUser(UserId) {
        let url = this._config.Server + "userMultipleDelete";
        console.log("before", UserId)
        let body = {
            usersId: UserId
        }
        console.log("before", UserId)
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }



    checkMail(email) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let body = email;
        let Url = API_URL + "/emailIdExistsVerificaiton";
        return this.http.patch(Url, body, options)
            .map(res => res.json())
    }
    checkNumber(number) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let body = number;
        let Url = API_URL + "/phoneNumberExistsVerificaton";
        return this.http.patch(Url, body, options)
            .map(res => res.json())
    }


    getAllLiked(offset, limit) {
        let url = this._config.Server + 'userActivityFilters?userType=1' + '&offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getAllSuperLikes(offset, limit) {
        let url = this._config.Server + 'userActivityFilters?userType=3' + '&offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getAllBoostUser(offset, limit) {
        let url = this._config.Server + 'userBoost?offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getAllDisLikedUsers(offset, limit) {
        let url = this._config.Server + 'userActivityFilters?userType=2' + '&offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    getProfileByContectNumber(contectNumber) {
        let url = this._config.Server + 'profile/contactNumber/' + contectNumber;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getProfileByEmail(email) {
        let url = this._config.Server + 'profile/email/' + email;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileById(id) {
        // var fdsfid = id._id
        //console.log("profileeeeeeeeeeeeee=================>", id)
        let url = this._config.Server + 'profile/' + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getDeviceLogById(id) {
        let url = this._config.Server + "device/" + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    getSearchFromLikeDates(user) {
        console.log("userdate", user)
        let url = this._config.Server + 'userActivityFilters?userType=1' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromDislikeDates(user) {
        console.log("userdate", user)
        let url = this._config.Server + 'userActivityFilters?userType=2' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromSuperLikeDates(user) {
        console.log("userdate", user)
        let url = this._config.Server + 'userActivityFilters?userType=3' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getSearchFromBoostDates(user) {
        console.log("userdate", user)
        let url = this._config.Server + 'userBoost?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        //  let url = this._config.Server + 'userBoost?offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchByBoostType(user) {
        let url = this._config.Server + 'userBoost?boostType=' + user;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
      }




}
function handleError(error: any) {

    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    return Observable.throw(errorMsg);
}
