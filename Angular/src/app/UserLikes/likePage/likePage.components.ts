import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { UsersService } from '../../users/users.service';
import { UserLikesService } from '../UserLikes.service';
// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'likePage',
    // encapsulation: ViewEncapsulation.None,
    styleUrls: ['./likePage.component.scss'],
    templateUrl: './likePage.components.html',
    providers: [UsersService, UserLikesService]

})
export class LikePageComponents {
    ActiveDates = [];


    public prefData = [];

    public rowsOnPage = 10;
    public p = 1;
    public totalusers;
    public UserDetails;
    public PageDetails;
    public preferences: any[];
    ActiveLikeDatesSearch = {


        "dateFrom": 0, "dateTo": 0,

    };
    ActiveDisLikeDatesSearch = {


        "dateFrom": 0, "dateTo": 0,

    };
    ActiveSuperLikeDatesSearch = {


        "dateFrom": 0, "dateTo": 0,

    };
    loader = true;
    public likeData: any[];
    public BoostuserData: any[];
    public disLikeData: any[];
    public superLikeData: any[];
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        LikeDate: "Friday, January 5, 2018 5:39:37 AM",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "--",
        about: "--",
        ProfileVideo: "",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "",
        likedByprofileVideo: "",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }
    public chatDetails: any;
    constructor(private _userService: UsersService, private route: ActivatedRoute, private router: Router
        , private _userLikesService: UserLikesService) {

    }
    ngOnInit() {
        // this.getDisLikes();
        this.getLikes(this.p);
        // this.getSuperLikes();
    }

    getLikes(p) {
        this._userLikesService.getAllLiked(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.likeData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("likes----------", data)
            }
            error => console.log(error)
        });
    }
    getDisLikes(p) {
        this._userLikesService.getAllDisLikedUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.disLikeData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("dislikes----------", data)
            }
            error => console.log(error)
        });
    }
    getSuperLikes(p) {
        this._userLikesService.getAllSuperLikes(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.superLikeData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("superlikes----------", data)
            }
            error => console.log(error)
        });
    }

    getboostUserData(p) {
        this._userLikesService.getAllBoostUser(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.BoostuserData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("bostusersssss----------", data)
            }
            error => console.log(error)
        });
    }


    backToUsers() {
        this.router.navigate(['/pages/users']);
    }
    //like Users Details Modal
    userDetailsModal(user) {
        this.prefData = [];
        console.log("user :: ", user);
        var searchPreferences = user.likedByMyPreferences
        searchPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.likedBydob, 'years');
        this.userDetails.Name = user.likedBy || "--";
        this.userDetails.PhoneNo = user.likedByContactNumber || " ";
        this.userDetails.Email = user.likedByEmail || " ";
        this.userDetails.Gender = user.likedBygender || "--";
        this.userDetails.about = user.likedByabout || "--";
        this.userDetails.DateOfBirth = user.likedBydob || 0;
        this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
        this.userDetails.LikeDate = user.likedOn || "--";
        this.userDetails.Height = user.likedByheight + " cm," + (parseFloat(((user.likedByheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.likedByprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.ProfilePhoto = user.likedByprofilePic;
        this.userDetails.OtherPhotos = user.likeByOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if ((user.likedBycity || user.likedBycountry)) {
            this.userDetails.RegisteredFrom = user.likedBycity + " ," || " ";
            this.userDetails.RegisteredFrom += user.likedBycountry || " ";
            // }
        }
        error => console.log(error)
        // });

    }
    //opponent like user Modal
    OpponentuserDetailsModal(user) {
        console.log("useropponnet :: ", user);
        this.prefData = [];
        // console.log("user :: ", user);
        var OpponentuserPreferences = user.opponentUserMyPreferences
        OpponentuserPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.opponentUserdob, 'years');
        this.userDetails.Name = user.opponentUser || "--";
        this.userDetails.PhoneNo = user.opponentUserContactNumber || "";
        this.userDetails.Email = user.opponentUserEmail || "";
        this.userDetails.Gender = user.opponentUsergender || "--";
        this.userDetails.about = user.opponentUserabout || "--";
        this.userDetails.DateOfBirth = user.opponentUserdob || 0;
        this.userDetails.LikeDate = user.likedOn || "--";
        this.userDetails.Height = user.opponentUserheight + " cm , " + (parseFloat(((user.opponentUserheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.opponentUserprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);

        this.userDetails.ProfilePhoto = user.opponentUserprofilePic || "";
        this.userDetails.OtherPhotos = user.opponentUserOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if ((user.opponentUsercity || user.opponentUsercountry)) {
            this.userDetails.RegisteredFrom = user.opponentUsercity + " ," || " ";
            this.userDetails.RegisteredFrom += user.opponentUsercountry || " ";
            // }



        }
        error => console.log(error)

    }
    //dislike user detail modal
    disLikeUserDetailsModal(user) {
        this.prefData = [];
        console.log("user :: ", user);
        var disLikeUserPreferences = user.unLikedByMyPreferences
        disLikeUserPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.unlikedBydob, 'years');
        this.userDetails.Name = user.unLikedBy || "--";
        this.userDetails.PhoneNo = user.unLikedByContactNumber || "";
        this.userDetails.Email = user.unLikedByEmail || "";
        this.userDetails.Gender = user.unlikedBygender || "--";
        this.userDetails.DateOfBirth = user.unlikedBydob || 0;
        this.userDetails.about = user.unlikedByabout || "--";
        this.userDetails.LikeDate = user.unLikedOn || "--";
        this.userDetails.Height = user.unlikedByheight + " cm , " + (parseFloat(((user.unlikedByheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.unlikeByprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);

        this.userDetails.ProfilePhoto = user.unlikedByprofilePic;
        this.userDetails.OtherPhotos = user.unlikedByOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.unLikedOn,
            this.userDetails.RegisteredFrom = "--";
        if (user.unlikedBycity || user.unlikedBycountry) {
            this.userDetails.RegisteredFrom = user.unlikedBycity + " ," || " ";
            this.userDetails.RegisteredFrom += user.unlikedBycountry || " ";
            // }

        }
        error => console.log(error)


    }
    //superlike user detail modal
    SuperLikeUserDetailsModal(user) {
        this.prefData = [];
        console.log("user :: ", user);
        var SuperLikeUserPreferences = user.superLikedByMyPreferences
        SuperLikeUserPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.superlikedBydob, 'years');
        this.userDetails.Name = user.superLikedBy || "--";
        this.userDetails.PhoneNo = user.superLikedByContactNumber || "";
        this.userDetails.Email = user.superLikedByEmail || "";
        this.userDetails.Gender = user.superlikedBygender || "--";
        this.userDetails.DateOfBirth = user.superlikedBydob || 0;
        this.userDetails.about = user.superlikedByabout || "--";
        this.userDetails.LikeDate = user.superLikedOn || "--";
        this.userDetails.Height = user.superlikedByheight + " cm , " + (parseFloat(((user.superlikedByheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.superlikedByprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.ProfilePhoto = user.superlikedByprofilePic;
        this.userDetails.OtherPhotos = user.superlikedByOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.unLikedOn,
            this.userDetails.RegisteredFrom = "--";
        if (user.superlikedBycity || user.superlikedBycountry) {
            this.userDetails.RegisteredFrom = user.superlikedBycity + " ," || " ";
            this.userDetails.RegisteredFrom += user.superlikedBycountry || " ";
        }
        error => console.log(error)

    }
    // oppenent user detail modal for dislike
    OpponentDisLikeuserDetailsModal(user) {
        this.prefData = [];
        // console.log("user :: ", user);
        var OpponentDisLikePreferences = user.unLikedOpponentUserMyPreferences
        OpponentDisLikePreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.unLikedOpponentUserdob, 'years');
        this.userDetails.Name = user.unLikedOpponentUser || "--";
        this.userDetails.PhoneNo = user.unLikedOpponentUserContactNumber || "";
        this.userDetails.Email = user.unLikedOpponentUserEmail || "";
        this.userDetails.about = user.unLikedOpponentUserabout || "--";
        this.userDetails.Gender = user.unLikedOpponentUsergender || "--";
        this.userDetails.DateOfBirth = user.unLikedOpponentUserdob || 0;
        this.userDetails.LikeDate = user.unLikedOn || "--";
        this.userDetails.Height = user.unLikedOpponentUserheight + " cm , " + (parseFloat(((user.unLikedOpponentUserheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.unLikedOpponentUserprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.ProfilePhoto = user.unLikedOpponentUserprofilePic;
        this.userDetails.OtherPhotos = user.unLikedOpponentUserOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if ((user.unLikedOpponentUsercity || user.unLikedOpponentUsercountry)) {
            this.userDetails.RegisteredFrom = user.unLikedOpponentUsercity + " ," || " ";
            this.userDetails.RegisteredFrom += user.unLikedOpponentUsercountry || " ";
            // }
        }
        error => console.log(error)

    }
    //opponent superlike user deatil modal
    OpponentSuperLikeuserDetailsModal(user) {
        this.prefData = [];
        console.log("user :: ", user);
        var OpponentSuperPreferences = user.superLikedOpponentUserMyPreferences
        OpponentSuperPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.superLikedOpponentUserdob, 'years');
        this.userDetails.Name = user.superLikedOpponentUser || "--";
        this.userDetails.PhoneNo = user.superLikedOpponentUserContactNumber || "";
        this.userDetails.Email = user.superLikedOpponentUserEmail || "";
        this.userDetails.Gender = user.superLikedOpponentUsergender || "--";
        this.userDetails.about = user.superLikedOpponentUserabout || "--";
        this.userDetails.DateOfBirth = user.superLikedOpponentUserdob || 0;
        this.userDetails.LikeDate = user.registeredTimestamp || "--";
        this.userDetails.Height = user.superLikedOpponentUserheight + " cm , " + (parseFloat(((user.superLikedOpponentUserheight * 0.032808399)).toString()).toFixed(2)) + "feet" || "--";
        this.userDetails.ProfileVideo = user.superLikedOpponentUserprofileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.ProfilePhoto = user.superLikedOpponentUserprofilePic;
        this.userDetails.OtherPhotos = user.superLikedOpponentUserOtherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
            this.userDetails.RegisteredFrom = "--";
        if ((user.superLikedOpponentUsercity || user.superLikedOpponentUsercountry)) {
            this.userDetails.RegisteredFrom = user.superLikedOpponentUsercity + " ," || " ";
            this.userDetails.RegisteredFrom += user.superLikedOpponentUsercountry || " ";
        }
        error => console.log(error)

    }

    boostUserDetailsModal(user) {
        console.log("-----------6^^][][][][]", user)

        $('#userDetails').modal('show');
        this._userLikesService
            .getProfileById(user.userId)
            .subscribe((user) => {
                console.log("+++++++++", user)
                if (user.data[0]) {
                    user = user.data[0];
                    this.prefData = []
                    var searchPreferences = user.myPreferences
                    searchPreferences.forEach(ele => {
                        if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Religious beliefs",
                                "selectedValue": ele.selectedValues[0],
                                "type": 3,
                                "pref_id": ele.pref_id,
                            })
                        }
                        else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Ethnicity",
                                "selectedValue": ele.selectedValues,
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Kids ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Work ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Job",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Education ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Politics ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Family Plans",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Highest Level Attended ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drinking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Smoking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Marijuana",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drugs",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Height ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }


                    });
                    this.userDetails.OtherPhotos.length = 0;
                    this.userDetails.OtherPhotos = [];
                    var years = moment().diff(user.dob, 'years');
                    this.userDetails.Name = user.firstName || "--";
                    this.userDetails.PhoneNo = user.contactNumber || "--";
                    this.userDetails.Email = user.email || "--";
                    this.userDetails.Gender = user.gender || "--";
                    this.userDetails.DateOfBirth = user.dob || 0;
                    this.userDetails.about = user.about || "--";
                    this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
                    this.userDetails.Height = user.height + " cm , " + user.heightInFeet + " feet" || "--";
                    this.userDetails.ProfileVideo = user.profileVideo || "";
                    jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
                    this.userDetails.ProfilePhoto = user.profilePic || "https://help.github.com/assets/images/help/profile/identicon.png";
                    this.userDetails.OtherPhotos = user.otherImages || [];
                    this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
                    this.userDetails.Preferences = user.favoritePreferences || [];
                    this.userDetails.Age = years || "--";
                    this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
                        this.userDetails.RegisteredFrom = "--";
                    if (user.address && (user.address.city || user.address.country)) {
                        this.userDetails.RegisteredFrom = user.address.city + " ," || " ";
                        this.userDetails.RegisteredFrom += user.address.country + " ," || " ";
                    }
                    $("#userDetails").modal("show");
                }
                error => console.log(error)
            });

    }
    gotoSearchLikes(term) {
        this._userLikesService.getUserByLikestext(term).subscribe(
            (data) => {
                this.totalusers = data.data.length;
                this.likeData = data.data;
                console.log("yyyyyyyyyyy", data.data);
            });
        if (term == "") {


            this.getLikes(this.p);

        }

    }
    gotoSearchDisLikes(term) {
        this._userLikesService.getUserByDisLikestext(term).subscribe(
            (data) => {
                this.totalusers = data.data.length;
                this.disLikeData = data.data;
                console.log("yyyyyyyyyyy", data.data);
            });
        if (term == "") {
            this.getDisLikes(this.p);
        }
    }
    gotoSearchSuperLikes(term) {
        this._userLikesService.getUserBySuperLikestext(term).subscribe(
            (data) => {
                this.totalusers = data.data.length;
                this.superLikeData = data.data;
                console.log("yyyyyyyyyyy", data.data);
            });
        if (term == "") {
            this.getSuperLikes(this.p);
        }
    }
    gotoSearchBoost(term) {
        this._userLikesService.getUserByBoosttext(term).subscribe(
            (data) => {
                this.totalusers = data.data.length;
                this.BoostuserData = data.data;
                console.log("BoostuserData", data.data);
            });
        if (term == "") {
            this.getDisLikes(this.p);
        }
    }
    FilterFromToLikes() {
        this._userLikesService.getSearchFromLikeDates(this.ActiveLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.totalusers = data.data.length;
                this.likeData = data.data;
            } else {
                this.likeData = [];
            }
        });
    }

    FilterFromTodisLikes() {
        this._userLikesService.getSearchFromDislikeDates(this.ActiveDisLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.totalusers = data.data.length;
                this.disLikeData = data.data;
            } else {
                this.disLikeData = [];
            }
        });
    }
    FilterFromToSuperLikes() {
        this._userLikesService.getSearchFromSuperLikeDates(this.ActiveSuperLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.totalusers = data.data.length;
                this.superLikeData = data.data;
            } else {
                this.superLikeData = [];
            }
        });
    }
    setLikesFromDate(date) {
        console.log("userdate", date)
        this.ActiveLikeDatesSearch.dateFrom = new Date(date).getTime();
        //this.ActiveDatesSearch.dateFrom= date;
        //this.ActiveDatesSearch.dateTo= date;
        this._userLikesService.getSearchFromLikeDates(this.ActiveLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.likeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.likeData = [];
            }
        });
        // console.log("userdate" , this.ActiveDates )
    }

    setLikesToDate(date) {
        console.log("userdate", date)
        this.ActiveLikeDatesSearch.dateTo = new Date(date).getTime();
        // this.ActiveDatesSearch.dateTo = date
        this._userLikesService.getSearchFromLikeDates(this.ActiveLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.likeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.likeData = [];
            }
        });
        // console.log("userdate" , this.ActiveDates )
    }
    setDisLikesFromDate(date) {
        this.ActiveDisLikeDatesSearch.dateFrom = new Date(date).getTime();

        // this.ActiveDatesSearch.dateFrom= date;
        //this.ActiveDatesSearch.dateTo= date;
        this._userLikesService.getSearchFromDislikeDates(this.ActiveDisLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.disLikeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.disLikeData = [];
            }
        });
    }

    setDisLikesDate(date) {
        this.ActiveDisLikeDatesSearch.dateTo = new Date(date).getTime();
        //this.ActiveDatesSearch.dateTo = date
        this._userLikesService.getSearchFromDislikeDates(this.ActiveDisLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.disLikeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.disLikeData = [];
            }
        });
    }
    setSuperLikesFromDate(date) {
        this.ActiveSuperLikeDatesSearch.dateFrom = new Date(date).getTime();

        // this.ActiveDatesSearch.dateFrom= date;
        //this.ActiveDatesSearch.dateTo= date;
        this._userLikesService.getSearchFromSuperLikeDates(this.ActiveSuperLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.superLikeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.superLikeData = [];
            }
        });
    }

    setSuperLikesToDate(date) {
        this.ActiveSuperLikeDatesSearch.dateTo = new Date(date).getTime();
        // this.ActiveDatesSearch.dateTo = date
        this._userLikesService.getSearchFromSuperLikeDates(this.ActiveSuperLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.superLikeData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.superLikeData = [];
            }
        });
    }



    setBoostFromDate(date) {
        this.ActiveDisLikeDatesSearch.dateFrom = new Date(date).getTime();
        this._userLikesService.getSearchFromBoostDates(this.ActiveDisLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.BoostuserData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.BoostuserData = [];
            }
        });
        if (date == "") {
            this.getboostUserData(this.p);
        }
    }

    setBoostToDate(date) {
        this.ActiveDisLikeDatesSearch.dateTo = new Date(date).getTime();
        this._userLikesService.getSearchFromBoostDates(this.ActiveDisLikeDatesSearch).subscribe((data) => {
            if (data.data) {
                this.BoostuserData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.BoostuserData = [];
            }
        });
        if (date == "") {
            this.getboostUserData(this.p);
        }
    }
    setBoostStatus(data) {

        this._userLikesService.getSearchByBoostType(data).subscribe((data) => {
            if (data.data) {
                this.BoostuserData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.BoostuserData = [];
            }
        });
    }
}