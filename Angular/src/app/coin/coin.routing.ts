 import { Routes, RouterModule }  from '@angular/router';

import {coinComponent } from './components/coin.component';
import { ModuleWithProviders } from '@angular/core';
import { ViewStatementsComponent} from './ViewStatements/ViewStatements.component';
export const coinroutes: Routes = [ 
 {
       path: '',
       component: coinComponent,
       data:{
         title: 'Plan'
       }
     
 },
 {
  path: ':ViewStatements',
    component: ViewStatementsComponent,
  data:{
    title: 'ViewStatements'
  }

}
];

export const routing: ModuleWithProviders = RouterModule.forChild(coinroutes);