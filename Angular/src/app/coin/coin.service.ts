import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
declare var API_URL: string;
@Injectable()
export class coinService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getCoins(offset, limit) {
        // let url = this._config.Server + "coin";
        let url = this._config.Server + 'coin?offset=' + offset + '&limit=' + limit;
        console.log("--------->", url)
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    uploadImgInServer(imgData){
        let url = this._config.Server + "UploadImgInServer";
        let body = {
            activeimage:imgData
        }

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }

    addCoins(value) {
        let url = this._config.Server + "coin";
        let body = JSON.stringify(value);
        // console.log("servicebody is===========>>>>>>", body);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    deletecoins(coinId) {
        let url = this._config.Server + "coins";
        let body = {
            coinId: coinId
        }
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    EditCoins(coinId, title, dataurl) {
        let url = this._config.Server + "coin";
        let body = {
            _id: coinId.id,
            coinTitle: title,
            imgURL: dataurl,
            // oldUrl:oldUrl
        }
        console.log("bodyyyyyyyyyyyyyyyyyyyyyyyyyyy", body)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getCoinPlan(offset, limit) {
        let url = this._config.Server + 'coinPlan?offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    EditCoinPlan(coinId, planName, unlock, cost, currency, symbol, planId) {
        let url = this._config.Server + "coinPlan";
        let body = {
            _id: coinId.id,
            planName: planName,
            noOfCoinUnlock: unlock,
            currency: currency,
            currencySymbole: symbol,
            planId: planId,
            cost: cost
        }
        console.log("bodyyyyyyyyyyyyyyyyyyyyyyyyyyy", body)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }


    deletecoinPlan(coinId) {
        let url = this._config.Server + "coinPlans";
        let body = {
            coinId: coinId
        }
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getCoinWallet(offset, limit) {
        let url = this._config.Server + 'coinWallet?offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchCoins(text) {
        let url = this._config.Server + "coin/" + text;

        console.log(text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getSearchCoinPlans(text) {
        let url = this._config.Server + "coinPlans/" + text;

        console.log(text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }

    addCoinPlans(value, unlock) {
        let url = this._config.Server + "coinPlan";
        //let body = JSON.stringify(value);
        let body = {
            planName: value.planName,
            cost: value.cost,
            currency: value.currency,
            currencySymbole: value.currencySymbole,
            planId: value.planId,
            noOfCoinUnlock: unlock

        }
        console.log("servicebody is===========>>>>>>", body);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getSearchCoinWallet(text) {
        let url = this._config.Server + "coinWallet/" + text;

        console.log(text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }

    getCustomerWallet(userid) {
        let url = this._config.Server + "walletCustomer/" + userid;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromCustWalletDates(user) {
        let url = this._config.Server + 'filterCoinWallet?txnType=' + user.txnType;
        // let url = this._config.Server + 'filterCoinWallet?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo + '&txnType=' + user.txnType +  '&coinType=' + user.coinType + '&text=' + user.text;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getcoinType(user) {
        let url = this._config.Server + 'filterCoinWallet?coinType=' + user.coinType;
        // let url = this._config.Server + 'filterCoinWallet?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo + '&txnType=' + user.txnType +  '&coinType=' + user.coinType + '&text=' + user.text;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromTOtoCoinWallet(user) {
        let url = this._config.Server + 'filterCoinWallet?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addCustomerWallet(value, userId) {
        let url = this._config.Server + "coinWallet";
        let body = {
            txnType: value.txnType,
            note: value.note,
            // currency:value.currency,
            // currencySymbole:value.currencySymbole,
            coinType: value.coinType,
            // cost:value.cost,
            coinAmount: value.coinAmount,
            // paymentType:value.paymentType,
            userId: userId
        }
        // let body = JSON.stringify(value,data);
        console.log("servicebody is===========>>>>>>", body);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getCoinPage(offset, limit) {
        let Url = this._config.Server + 'coin?offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
    getCoinPlanPage(offset, limit) {
        let Url = this._config.Server + 'coinPlan?offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
    getCoinWalletPage(offset, limit) {
        let Url = this._config.Server + 'coinWallet?offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
    getCustomerWalletPage(userid, offset, limit) {
        let Url = this._config.Server + 'walletCustomer/' + userid + '?offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
}



