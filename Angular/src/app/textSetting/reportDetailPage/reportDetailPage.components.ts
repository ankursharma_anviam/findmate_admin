import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { TextSettingService } from '../textSetting.service';

// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'reportDetailPage',
    // encapsulation: ViewEncapsulation.None,
    templateUrl: './reportDetailPage.components.html',
    providers: [TextSettingService]

})
export class ReportDetailPageComponents {

    loader = true;
    data: any ;
    _id: any = "";
    i = 0;
    reason = "";

    public chatDetails: any;
    constructor(private _TextSettingService: TextSettingService, private route: ActivatedRoute, private router: Router) {
        this.route.params.subscribe(params => {
            let _id = params['id'];
            console.log("id - ", _id)
            this._id = _id;
            this._TextSettingService
                .getReportReasons(_id)
                .subscribe((data) => {
                    console.log("data", data);
                    if (data.data) {
                        this.data = data.data;
                        if (data.reason) {
                            this.reason = data.reason
                        }
                    }
                    error => console.log(error)
                });
        });
    }
    AddLanguage() {
        $("#addReason").modal("show");
    }
    SaveReason() {
        $("#addReason").modal("hide");
        // let reason = $("#reason").val();
        let languageName = $("#langeugeName").val();
        let localisedString = $("#localisedString").val();

        this.data.push({ "languageName": languageName, "localisedString": localisedString });

        this._TextSettingService.updateReason({ data: this.data, _id: this._id })
            .subscribe((dataA) => { });

        $("#langeugeName").val("");
        $("#localisedString").val("");
    }
    Edit(obj, i) {
        this.i = i;
        console.log("object ", obj);
        $("#EditReason").modal("show");
        $("#langeugeNameEdit").val(obj.languageName);
        $("#localisedStringEdit").val(obj.localisedString);
    }
    UpdateReason() {
        $("#EditReason").modal("hide");
        let languageName = $("#langeugeNameEdit").val();
        let localisedString = $("#localisedStringEdit").val();

        this.data[this.i]["languageName"] = languageName;
        this.data[this.i]["localisedString"] = localisedString;

        this._TextSettingService.updateReason({ data: this.data, _id: this._id })
            .subscribe((dataA) => { });

    }
    Remove(i) {
        let _id = this.data[i]._id;
        this.data.splice(i, 1);

        this._TextSettingService.updateReason({ data: this.data, _id: this._id })
            .subscribe((dataA) => { });
    }
}