import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


//=================== importing Matches Service   =============
import { MatchesService } from '../matches.service';
import { UsersService } from '../../users/users.service';
import { ProfilesService } from '../../profiles/profile.service';
declare var swal: any;


@Component({
    selector: 'matches',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./matches.component.scss'],
    templateUrl: './matches.component.html',
    providers: [MatchesService, UsersService, ProfilesService]
})

export class MatchesComponent implements OnInit, AfterViewInit {
    addResonces: FormGroup;
    editResonces: FormGroup;
    loader = true;
    oldReason: any;
    newReason: any;
    data: any;
    id: any
    lang: any;
    deleteUserId: any;
    obj: any;
    reson: any;
    reportResonceName: any;
    public config: any;
    public configFn: any;

    constructor(private _matchesService: MatchesService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addResonces = fb.group({
            'reportResonceName': ['', Validators.required],
        });

        this.editResonces = fb.group({
            'reportResonceName': ['', Validators.required],
            "newReason_id": [''],
        });


    }

    ngOnInit() {
        this._matchesService.getData().subscribe(result => {
            if (result.data) {
                this.data = result.data
            }
        });

    }

    submitForm(value) {
        console.log("val", value._value.reportResonceName)
        this._matchesService.addResonce(value._value.reportResonceName).subscribe(
            (result) => {
                if (result.code != 200) {
                    this.ngOnInit();
                    swal("Success!", "ReportReasons Added Successfully!", "success");

                } else {

                }
                jQuery('#addResonce').modal('hide');



            })
    }



    showModalDelete(_id) {
        this._matchesService
            .deleteUser(_id)
            .subscribe((result) => {
                if (result.code != 200) {
                    swal("Success!", "ReportReasons Deleted!", "success");
                    this.ngOnInit();

                } else {

                }

            })
    }


    editResonnce(reason) {
        this.obj = reason.reason;
        this.id = reason._id

    }

    updatedReason(reason) {
        var reasons = reason._value.reportResonceName
        this._matchesService.editReason(reasons, this.id).subscribe(
            result => {
                if (result.code != 200) {
                    swal("Success!", "ReportReasons Edited!", "success");
                    this.ngOnInit();

                } else {
                }

                jQuery('#editCategory').modal('hide');

            }
        )
    }

}
