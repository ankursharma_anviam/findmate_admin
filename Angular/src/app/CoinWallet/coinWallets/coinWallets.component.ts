import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import { CoinWalletService } from '../CoinWallet.service';

import { ActivatedRoute } from '@angular/router';
import {
    AppState
} from "../../app.state";

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'coinWallets',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./coinWallets.component.scss'],
    templateUrl: './coinWallets.component.html',
    providers: [CoinWalletService]
})

export class coinWalletsComponent implements OnInit {
    public CoinData: any[];
    public coinWallet: any[];
    public coinconf: any[];
    public id: any;
    public deleteUser: any = [];
    CoinConfig: any
    public data = {}
    public olditle: any
    public editconfig: any
    public dollarCoinData:any
    AddCoinData = [];
    perMsgWithoutMatch = {};
    boostProfileForADay = {};
    callDateInitiated = {};
    public addcoins: FormGroup;
    ActiveDates = [];
    inPersonDateInitiated = {}
    public rowsOnPage = 10;
    public p = 1;
    public totalusers;
    pushData = [];
    public array = []
    public addcoinconfig: FormGroup;
    public editcoinconfig: FormGroup;

    ActiveDatesSearch = {
        "dateFrom": 0, "dateTo": 0,
    };




    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef,
        private _state: AppState, private router: Router, private _coinWalletService: CoinWalletService, private route: ActivatedRoute,
        private _zone: NgZone) {

        this.addcoins = fb.group({
            "coinTitle": ['', Validators.required],
            "Coins": ['', Validators.required],
            "coinTitle1": ['', Validators.required],
            "Coins1": ['', Validators.required],
            "coinTitle2": ['', Validators.required],
            "Coins2": ['', Validators.required],
            "coinTitle3": ['', Validators.required],
            "Coins3": ['', Validators.required],

        });


        this.addcoinconfig = fb.group({
            'coinTitle': ['', Validators.required],
            'title': ['', Validators.required],
            'amount': ['', Validators.required],

        });
        this.editcoinconfig = fb.group({
            'coinTitle': ['', Validators.required],
            'title': ['', Validators.required],
            'amount': ['', Validators.required],

        });


    }

    ngOnInit() {
        this.getCoinWalletData(this.p);
        this.getCoins();

    }

    getCoinWalletData(p) {
        console.log("----------->", p - 1, this.rowsOnPage)
        this._coinWalletService.getCoinWallet(p - 1, this.rowsOnPage).subscribe((data) => {
            this.coinWallet = data.data
            this.totalusers = data.totalCount;
            this.p = p;
            console.log("coinWallet==============>", data, this.totalusers)


        })
    }
    getCoinConfig() {
        this.array = [];
        this._coinWalletService.getCoinConfigs().subscribe((data) => {
            this.CoinConfig = data.data[0]
            for (var xx in this.CoinConfig) {
                var ff = this.CoinConfig[xx];
                if (ff) {
                    for (let x1 in ff) {
                        var s = x1;

                        var dd = {
                            title: xx,
                            key: s,
                            value: ff[x1]
                        }
                        this.array.push(dd)
                    }
                }
            }
            // console.log("getCoinConfigs==============>", this.array)

        })
    }

    store(value) {
        this._coinWalletService.addPGWallet(value).subscribe((result) => {
            this.array = [];
            swal("Success!", "coin config Successfully!", "success");
            jQuery('#addconf').modal('hide');
            this.getCoinConfig();
            this.addcoinconfig.reset();

        })
    }

    backToUsers() {
        this.router.navigate(['pages/users'])
    }
    getCoins() {
        this._coinWalletService.getCoins().subscribe(
            result => {

                this.CoinData = result.data;
                // console.log("CoinData==============>", this.CoinData)

            }
        )
    }
    getdollarToCoin() {
        this._coinWalletService.getdollarToCoin().subscribe(
            result => {

                this.dollarCoinData = result.data;
                console.log("getdollarToCoin==============>", this.CoinData)

            }
        )
    }


    resetForm() {
        this.addcoins.reset();
    }

    FilterFromTo() {
        this._coinWalletService.getSearchFromCoinWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {

                this.coinWallet = data.data;
            } else {
                this.coinWallet = [];
            }
        });
    }
    SearchByFromDate(date) {
        this.ActiveDatesSearch.dateFrom = new Date(date).getTime();
        this._coinWalletService.getSearchFromCoinWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
            } else {
                this.coinWallet = [];
            }
        });
    }

    SearchByToDate(date) {
        this.ActiveDatesSearch.dateTo = new Date(date).getTime();
        this._coinWalletService.getSearchFromCoinWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
            } else {
                this.coinWallet = [];
            }
        });
    }

    setTxnType(data) {

        this._coinWalletService.getSearchByTxn(data).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
            } else {
                this.coinWallet = [];
            }
        });
    }

    SearchByText(data) {


        this._coinWalletService.getSearchByText(data).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
            } else {
                this.coinWallet = [];
            }
        });
    }


    DeleteCoinconf(title) {


        this._coinWalletService
            .deleteCoinConfig(title).subscribe((data) => {
                if (data) {
                    swal("Success!", "CoinConfig Deleted!", "success");
                    this.array = [];
                    this.getCoinConfig();
                }
            });
    }
    EditCoinConf(title) {
        this.data = title
        this.olditle = title.title

    }
    editCoinConfig(CoinConfig) {
        this._coinWalletService.editcoinconfig(CoinConfig, this.olditle).subscribe(
            result => {
                if (result.code !== 200) {
                    swal("Success!", "CoinConfig Edited Successfully!", "success");
                    jQuery('#editcoinconf').modal('hide');
                    this.array = [];
                    this.getCoinConfig();

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                this.editcoinconfig.reset();
            }
        )
    }
    selectAllCoinCheckBox() {
        this.deleteUser = [];
        this.array.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push(obj.title)

            }

        });
        console.log("===>>", this.deleteUser)

    }






}








