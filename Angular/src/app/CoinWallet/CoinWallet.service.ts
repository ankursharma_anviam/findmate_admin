import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class CoinWalletService {

  constructor(private http: Http, public _config: Configuration) {
  }
  getCoinWallet(offset, limit) {
    let url = this._config.Server + 'walletPG?offset=' + offset + '&limit=' + limit;
    console.log('--------->',url)
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getCoins() {
    let url = this._config.Server + "coin";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getCoinConfigs() {
    let url = this._config.Server + "coinConfigs";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getSearchFromCoinWallet(user) {
    let url = this._config.Server + 'walletPGByText?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getSearchByText(user) {
    let url = this._config.Server + 'walletPGByText?text=' + user;
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getSearchByTxn(user) {
    let url = this._config.Server + 'walletPGByText?txnType=' + user;
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getdollarToCoin(){
    let url = this._config.Server + "dollartoCoin";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  addPGWallet(value) {
    let url = this._config.Server + "coinConfig";
    let body = {
      title: value.title,
      coinTitle: value.coinTitle,
      amount: value.amount
    }
    // let body = JSON.stringify(value,data);
    // console.log("servicebody is===========>>>>>>", body);
    return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
  }
  
  deleteCoinConfig(value) {
    let url = this._config.Server + "coinConfigDelete";
    let body = {
      field: value
    }
    // console.log("servicebody is===========>>>>>>", body);
    return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
  }
  editcoinconfig(coinconf,oldfield) {
    let url = this._config.Server + "coinConfigUpdate";
    let body = {
      title: coinconf.title,
      coinTitle: coinconf.coinTitle,
      amount: coinconf.amount,
      oldfield:oldfield
    }
    // console.log("servicebody is===========>>>>>>", body);
    return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
  }

}

// this could also be a private method of the component class
function handleError(error: any) {
  // log error
  // could be something more sofisticated
  let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
  console.error(errorMsg);

  // throw an application level error
  return Observable.throw(errorMsg);
}
