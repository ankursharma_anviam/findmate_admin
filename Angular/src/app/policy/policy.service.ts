import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class PolicyService {

    constructor(private http: Http, public _config: Configuration) {
    }



    updatePolicyFile(data) {

        let url = this._config.Server + 'policy';
        return this.http.put(url, data, { headers: this._config.headers }).map(res => res.json());
    }

    getPolicyFile() {
        let url = this._config.Server + 'policy';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

}


function handleError(error: any) {
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    return Observable.throw(errorMsg);
}
