import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { DatesService } from '../dates.service';

declare var $: any;

@Component({
    selector: 'activeDates',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./activeDates.component.scss'],
    templateUrl: './activeDates.component.html',
    providers: [DatesService]
})


export class ActiveDatesComponent {
    public rowsOnPage = 10;
    public p = 1;
    public totalusers;
    public prefData = [];
    ActiveDates = [];
    CompletedDates = [];
    ExpiredDates = [];

    ActiveDatesSearch = {
        "dateFrom": 0, "dateTo": 0,
        "dateType": "All", "rating": "All", "status": "All",
        "text": "All"
    };
    CompleteDatesSearch = {
        "dateFrom": 0, "dateTo": 0,
        "dateType": "All", "rating": "All", "status": "All",
        "text": "All"
    };
    ExpiredDatesSearch = {
        "dateFrom": 0, "dateTo": 0,
        "dateType": "All", "rating": "All", "status": "All",
        "text": "All"
    };
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        about: "--",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }
    constructor(private _appConfig: AppConfig, private router: Router, private _DatesService: DatesService) {

        // this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
        //     if (data.data) {
        //         this.ActiveDates = data.data;
        //         console.log("getSearchFromActiveDates",data)
        //     } else {
        //         this.ActiveDates = [];
        //     }
        // });

        // this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
        //     if (data.data) {
        //         this.CompletedDates = data.data;
        //         console.log("getSearchFromCompleteDates",data)

        //     } else {
        //         this.CompletedDates = [];
        //     }
        // });

        // this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
        //     if (data.data) {
        //         this.ExpiredDates = data.data;
        //         console.log("getSearchFromExpiredDates",data)

        //     } else {
        //         this.ExpiredDates = [];
        //     }
        // });
    }

    ngOnInit() {

        this.activeDate(this.p);

    }
    activeDate(p) {
        
        this._DatesService.getSearchFromActiveDatesData(this.ActiveDatesSearch, p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
                console.log("======ActiveDates", this.ActiveDates)
                this.totalusers = data.totalCount;
                this.p = p;
            } else {
                this.ActiveDates = [];
            }
        });

    }
    completeDates(p) {
        this._DatesService.getSearchFromCompleteDatesData(this.CompleteDatesSearch, p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.CompletedDates = data.data;
                console.log("======CompletedDates", this.CompletedDates)
                this.totalusers = data.totalCount;
                this.p = p;
            } else {
                this.CompletedDates = [];
            }
        });


    }

    expiredDates(p) {
        this._DatesService.getSearchFromExpiredDatesData(this.ExpiredDatesSearch, p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
                console.log("======ExpiredDates", this.ExpiredDates)
                this.totalusers = data.totalCount;
                this.p = p;
            } else {
                this.ExpiredDates = [];
            }
        });

    }
    /* start active Dates form */
    setActiveFromDate(date) {
        this.ActiveDatesSearch.dateFrom = new Date(date).getTime();
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
                this.totalusers = data.length;

            } else {
                this.ActiveDates = [];
            }
        });
    }
    setActiveToDate(date) {
        this.ActiveDatesSearch.dateTo = new Date(date).getTime();
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
            } else {
                this.ActiveDates = [];
            }
        });
    }
    setActiveDateType(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.dateType = "All";
        } else {
            this.ActiveDatesSearch.dateType = data;
        }

        this.ActiveDatesSearch.dateType = data;
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
            } else {
                this.ActiveDates = [];
            }
        });
    }
    setActiveDateRating(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.rating = "All";
        } else {
            this.ActiveDatesSearch.rating = data;
        }
        this.ActiveDatesSearch.rating = data;
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
                this.totalusers = data.length;
                console.log("============== this.totalusers", this.totalusers , data)
            } else {
                this.ActiveDates = [];
            }
        });
    }
    setActiveDateStatus(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.status = "All";
        } else {
            this.ActiveDatesSearch.status = data;
        }
        this.ActiveDatesSearch.status = data;
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
            } else {
                this.ActiveDates = [];
            }
        });

    }
    setActiveDateText(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.text = "All";
        } else {
            this.ActiveDatesSearch.text = data;
        }
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
            } else {
                this.ActiveDates = [];
            }
        });
    }
    resetActiveDates() {
        this.ActiveDatesSearch = {
            "dateFrom": 0, "dateTo": 0,
            "dateType": "All", "rating": "All", "status": "All",
            "text": "All"
        };
        this._DatesService.getSearchFromActiveDates(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
            } else {
                this.ActiveDates = [];
            }
        });
    }
    openActiveDetailPage(_id) {
        console.log("===>", _id);
        this.router.navigate(['/pages/activeDatesDetailPage/' + _id]);
    }
    /* end active Dates form */


    /* start completed Dates form */
    setCompletedFromDate(date) {

        this.CompleteDatesSearch.dateFrom = new Date(date).getTime();
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });
    }
    setCompletedToDate(date) {
        this.CompleteDatesSearch.dateTo = new Date(date).getTime();
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });
    }
    setCompletedDateType(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.dateType = "All";
        } else {
            this.ActiveDatesSearch.dateType = data;
        }
        this.CompleteDatesSearch.dateType = data;
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });
    }
    setCompletedDateRating(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.rating = "All";
        } else {
            this.ActiveDatesSearch.rating = data;
        }
        this.CompleteDatesSearch.rating = data;
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            if (data.data) {
                this.CompletedDates = data.data;
                this.totalusers = data.data.length;
                console.log("============== this.totalusers", this.totalusers , data)
            } else {
                this.CompletedDates = [];
            }
        });
    }
    setCompletedDateStatus(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.status = "All";
        } else {
            this.ActiveDatesSearch.status = data;
        }
        this.CompleteDatesSearch.status = data;
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            console.log("setCompletedDateStatus ", data)
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });

    }
    setCompletedDateText(data) {
        if (data == "" || data == null) {
            this.CompleteDatesSearch.text = "All";
        } else {
            this.CompleteDatesSearch.text = data;
        }
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            console.log("setCompletedDateText ", data)
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });
    }
    resetCompletedDates() {
        this.CompleteDatesSearch = {
            "dateFrom": 0, "dateTo": 0,
            "dateType": "All", "rating": "All", "status": "All",
            "text": "All"
        };
        this._DatesService.getSearchFromCompleteDates(this.CompleteDatesSearch).subscribe((data) => {
            console.log("resetCompletedDates ", data)
            if (data.data) {
                this.CompletedDates = data.data;
            } else {
                this.CompletedDates = [];
            }
        });
    }



    userDetailsModal(userID) {
        this._DatesService
            .getProfileById(userID)
            .subscribe((user) => {
                if (user.data[0]) {
                    user = user.data[0];
                    this.prefData = []
                    //  console.log("+++++++++",user)
                    var searchPreferences = user.myPreferences
                    //    console.log("-----------6^^][][][][]", this.prefData)
                    searchPreferences.forEach(ele => {
                        if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Religious beliefs",
                                "selectedValue": ele.selectedValues[0],
                                "type": 3,
                                "pref_id": ele.pref_id,
                            })
                        }
                        else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Ethnicity",
                                "selectedValue": ele.selectedValues,
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Kids ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Work ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Job",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Education ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Politics ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Family Plans",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Highest Level Attended ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drinking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Smoking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Marijuana",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drugs",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Height ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }


                    });
                    this.userDetails.OtherPhotos.length = 0;
                    this.userDetails.OtherPhotos = [];
                    var years = moment().diff(user.dob, 'years');
                    this.userDetails.Name = user.firstName || "--";
                    this.userDetails.PhoneNo = user.contactNumber || "--";
                    this.userDetails.Email = user.email || "--";
                    this.userDetails.Gender = user.gender || "--";
                    this.userDetails.DateOfBirth = user.dob || 0;
                    this.userDetails.about = user.about || "--";
                    this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
                    this.userDetails.Height = user.height + " cm , " + user.heightInFeet + " feet" || "--";
                    this.userDetails.ProfileVideo = user.profileVideo || "";
                    jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
                    this.userDetails.ProfilePhoto = user.profilePic || "https://help.github.com/assets/images/help/profile/identicon.png";
                    this.userDetails.OtherPhotos = user.otherImages || [];
                    this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
                    this.userDetails.Preferences = user.favoritePreferences || [];
                    this.userDetails.Age = years || "--";
                    this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
                        this.userDetails.RegisteredFrom = "--";
                    if (user.address && (user.address.city || user.address.country)) {
                        this.userDetails.RegisteredFrom = user.address.city + " ," || " ";
                        this.userDetails.RegisteredFrom += user.address.country + " ," || " ";
                    }
                    $("#userDetails").modal("show");
                }
                error => console.log(error)
            });

    }


    openCompletedDetailPage(_id) {
        console.log("===>", _id);
        this.router.navigate(['/pages/completedDatesDetailPage/' + _id]);
    }
    /* end completed Dates form */

    /* start expired Dates form */
    setExpiredFromDate(date) {
        this.ExpiredDatesSearch.dateFrom = new Date(date).getTime();
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    setExpiredToDate(date) {
        this.ExpiredDatesSearch.dateTo = new Date(date).getTime();
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    setExpiredDateType(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.dateType = "All";
        } else {
            this.ActiveDatesSearch.dateType = data;
        }
        this.ExpiredDatesSearch.dateType = data;
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    setExpiredDateRating(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.rating = "All";
        } else {
            this.ActiveDatesSearch.rating = data;
        }
        this.ExpiredDatesSearch.rating = data;
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    setExpiredDateStatus(data) {
        if (data == "" || data == null) {
            this.ActiveDatesSearch.status = "All";
        } else {
            this.ActiveDatesSearch.status = data;
        }
        this.ExpiredDatesSearch.status = data;
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });

    }
    setExpiredDateText(data) {
        if (data == "" || data == null) {
            this.ExpiredDatesSearch.text = "All";
        } else {
            this.ExpiredDatesSearch.text = data;
        }
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    resetExpiredDates() {
        this.ExpiredDatesSearch = {
            "dateFrom": 0, "dateTo": 0,
            "dateType": "All", "rating": "All", "status": "All",
            "text": "All"
        };
        this._DatesService.getSearchFromExpiredDates(this.ExpiredDatesSearch).subscribe((data) => {
            if (data.data) {
                this.ExpiredDates = data.data;
            } else {
                this.ExpiredDates = [];
            }
        });
    }
    // getActivePage(p1) {
    //     this._DatesService
    //         .getactiveUserPage(p1 - 1, this.rowsOnPage1).subscribe((data) => {
    //             {
    //                 this.p1 = p1;
    //             }
    //         });
    // }
    // getCoplatePage(p2) {
    //     this._DatesService
    //         .getcomplateUserPage(p2 - 1, this.rowsOnPage2).subscribe((data) => {
    //             {
    //                 this.p2 = p2;
    //             }
    //         });
    // }
    // getexpirePage(p3) {
    //     this._DatesService
    //         .getexpireUserPage(p3 - 1, this.rowsOnPage3).subscribe((data) => {
    //             {
    //                 this.p3 = p3;
    //             }
    //         });
    // }
    openExpiredDetailPage(_id) {
        console.log("===>", _id);
        this.router.navigate(['/pages/expiredDatesDetailPage/' + _id]);
    }
    /* end expired Dates form */




}