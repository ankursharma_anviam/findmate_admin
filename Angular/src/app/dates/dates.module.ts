import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import {Ng2PaginationModule} from 'ng2-pagination'; 
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyDatePickerModule } from 'mydatepicker';
// import { DirectivesModule } from "../theme/directives/directives.module";

import { ActiveDatesComponent } from './activeDates/activeDates.component';
import { activeDatesDetailPageComponents} from "./activeDatesDetailPage/activeDatesDetailPage.components";
import { completedDatesDetailPageComponents} from "./completedDatesDetailPage/completedDatesDetailPage.components";
import {expiredDatesDetailPageComponents} from "./expiredDatesDetailPage/expiredDatesDetailPage.components";

import { CKEditorModule } from 'ng2-ckeditor';

// export const routes = [
//   { path: '', component: 'ng2-adminconfig', pathMatch: 'full' },
//   { path: 'adminconfig', component: AdminConfigComponent, data: { breadcrumb: 'Ng2 Adminconfig' } }

// ];
export const routes = [
  // { path: '', redirectTo: 'policy', pathMatch: 'full' },
  // { path: 'policy', component: PolicyComponent, data: { breadcrumb: 'policy' } }
];
@NgModule({
  imports: [
    Ng2PaginationModule,
    MyDatePickerModule,
    CommonModule,
    ChartsModule,
    // DirectivesModule,
    RouterModule,
    
    CKEditorModule,
    RouterModule.forChild(routes),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ActiveDatesComponent,
    activeDatesDetailPageComponents,
    completedDatesDetailPageComponents,
    expiredDatesDetailPageComponents
  ],


})
export class DatesModule {

}