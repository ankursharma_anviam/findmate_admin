import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';

import {
    DashboardService
} from '../dashboard.service';
@Component({
    selector: 'dynamic-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chart.html',
    providers: [ DashboardService]
})

export class DynamicChartComponent {
    public config: any;
    public configFn: any;
    public dashboardData: any;

    constructor( private _dashboardService: DashboardService) {
    }
    ngOnInit() {
        this.getuserActivitesDashboard()
    }
    private getuserActivitesDashboard(): void {
        this._dashboardService
            .getuserActivitesDashboard()
            .subscribe((result) => {
                {
                    if (result.data) {
                        this.dashboardData = result.data.report;
                        jQuery(".iframeListuseractivity").attr("src", this.dashboardData);                
                    } else {
                    } 

                }

            })
    }

}
