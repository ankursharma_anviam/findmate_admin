import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Configuration } from '../app.constant';
@Injectable()
export class DashboardService {

  constructor(private http: Http, public _config: Configuration) {
  }

  getDashboardData() {
    let url = this._config.Server + "admin/dashbord/report/" + "227";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }

  getuserActivitesDashboard() {
    let url = this._config.Server + "admin/dashbord/report/" + "228";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getuserMatchDashboard() {
    let url = this._config.Server + "admin/dashbord/report/" + "138";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getuserDeviceDashboard() {
    let url = this._config.Server + "admin/dashbord/report/" + "230";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
  getuserAppEarningsDashboard() {
    let url = this._config.Server + "admin/dashbord/report/" + "231";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

  }
  getuserCoinsDashboard() {
    let url = this._config.Server + "admin/dashbord/report/" + "229";
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

  }
}



