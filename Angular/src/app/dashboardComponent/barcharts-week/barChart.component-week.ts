import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../app.config";
import {
    DashboardService
} from '../dashboard.service';
@Component({
    selector: 'dynamic-chartWeek',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chartweek.html',
    providers: [ DashboardService]

})

export class DynamicChartComponentWeek {
        
    public config: any;
    public configFn: any;
    public dashboardData: any;

    constructor( private _dashboardService: DashboardService) {
    }
    ngOnInit() {
        this.getuserAppEarningsDashboard()
    }
    private getuserAppEarningsDashboard(): void {
        this._dashboardService
            .getuserAppEarningsDashboard()
            .subscribe((result) => {
                {
                    if (result.data) {
                        this.dashboardData = result.data.report;
                        jQuery(".iframeListuseractivity").attr("src", this.dashboardData);                
                    } else {
                    } 

                }

            })
    }

}
