import { Component ,OnInit , Input} from '@angular/core';
import { AppConfig } from "../../app.config";

@Component({
  selector: 'DevType',
   template: `
    <div style="display: block">
  <canvas baseChart
          [data]="pieChartData"
          [labels]="pieChartLabels"
          [options]="lineChartOptions"
          [colors]="colors"
          [chartType]="pieChartType"
        
          (chartClick)="chartClicked($event)"></canvas>     
</div>
  `,
})
//  (chartHover)="chartHovered($event)"

export class DevType{
    @Input() browserCount:any;
      @Input() iosCount:any;
       @Input() androidCount:any;
 
  public pieChartLabels:string[] = ['IOS','Andriod','Browser'];
 
  public pieChartData:number[];
  public pieChartType:string = 'doughnut';


     ngOnInit() {
  this.pieChartData =[this.iosCount,this.androidCount,this.browserCount];
 
    }


    
  public colors: any[] = [
      { 
   backgroundColor:["#46bfbd" ,"rgb(255, 157, 63)","rgb(254, 91, 63)"] 
      }];
  public lineChartOptions:any = {
    legend : {
        labels : {
          fontColor : '#aaa'  
        }
    }
};

}

// ,"rgb(254, 194, 63)" ,"rgb(254, 177, 65)"

//