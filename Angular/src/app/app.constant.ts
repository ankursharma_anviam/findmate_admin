import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';




@Injectable()
export class Configuration {
    Server: string = "https://xxxxxxxx.com/";
    
    authToken = localStorage.getItem('adminToken');
    headers = new Headers({ 'Content-Type': 'application/json' });
    constructor() {
        this.headers.append('authorization', localStorage.getItem('adminToken'));
        this.headers.append('lang', 'en');
    }
    setToken() {
        this.headers.set('authorization', localStorage.getItem('adminToken'));
        this.headers.set('lang', 'en');
    }
}