import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

import { Licensesservices } from "../licenses.service";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'licenses',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./licenses.component.scss'],
    templateUrl: './licenses.component.html',
    providers: [Licensesservices]
})


export class licensesComponent {
    ckeditorContent: any;
    pageForm: FormGroup;
    fileName: any;
    type = 1;
    data1: any;
    dataURI: any;
    public_id: any;
    cropperSettings: CropperSettings;
    croppedWidth: number;
    croppedHeight: number;
    cloudinaryImage: any;
    private answer: any = '';
    imgagefile: any;
    adminURL = 'https://xxxxxxxxxx.com/datum_2.0-admin/Licenses.html';
    private images: any = [];
    private uploaded: boolean = false;
    public config = {
        uiColor: '#F0F3F4',
        height: '600',
    };

    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    imageSrc: any;
    constructor(private _appConfig: AppConfig, private router: Router,
        vcRef: ViewContainerRef, fb: FormBuilder, private _Licensesservices: Licensesservices) {
        this.pageForm = fb.group({
            'fileName': "",
        });
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.noFileInput = true;
        this.data1 = {};
        this.ckeditorContent = `Licenses..dsadsadsad..`;

        this._Licensesservices.getTermsAndConditionsFile().subscribe(result => {
            console.log(" TermsAndConditions result is : ", result.data)
            if (result.data) {
                this.ckeditorContent = result.data
            }

        });

    }
    backToPrivicy() {
        this.router.navigate(['/pages/users']);
    }
    ngOnInit() {

    }



    gotoSaveFile() {
        console.log("source", this.ckeditorContent);

        this._Licensesservices.updateTermsAndConditionsFile({ data: this.ckeditorContent }).subscribe(result => {
            this.ngOnInit();
            if (result.code != 200) {
                swal("Success!", "File Saved Successfully!", "success");
            } else {
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        });
    }

}