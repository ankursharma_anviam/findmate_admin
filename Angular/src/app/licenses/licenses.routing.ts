import { Routes, RouterModule } from '@angular/router';

import { licensesComponent } from './licenses/licenses.component';

import { ModuleWithProviders } from '@angular/core';

export const licensesroutes: Routes = [
  {
    path: '', component: licensesComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(licensesroutes);