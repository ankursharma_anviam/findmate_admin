const helpers = require('./helpers');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.js'); 

const DefinePlugin = require('webpack/lib/DefinePlugin');

const ENV = process.env.ENV = process.env.NODE_ENV = 'development';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 9000;
const API_URL = process.env.API_URL = 'https://xxxxxxxxxx.com/'; // globally declared for Api

const METADATA = webpackMerge(commonConfig.metadata, {
  host: HOST,
  port: 4200,
    API_URL: API_URL,
  ENV: ENV
});


module.exports = webpackMerge(commonConfig, {

  devtool: 'cheap-module-eval-source-map',

  output: {

    path: helpers.root('dist'),

    filename: '[name].bundle.js',

    sourceMapFilename: '[name].map',

    chunkFilename: '[id].chunk.js'

  },

  plugins: [
   
    new DefinePlugin({
       'API_URL': JSON.stringify(METADATA.API_URL),
      'ENV': JSON.stringify(METADATA.ENV),
      'process.env': {
         'API_URL' : JSON.stringify(METADATA.API_URL),
        'ENV': JSON.stringify(METADATA.ENV),
        'NODE_ENV': JSON.stringify(METADATA.ENV)
      }
    })
  ],

  devServer: {
    port: METADATA.port,
    host: METADATA.host,
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },

  node: {
    global: true,
    crypto: 'empty',
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false
  }

});
