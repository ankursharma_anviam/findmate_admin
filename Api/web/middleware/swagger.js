
const inert = require('inert');
const vision = require('vision');
const  HapiSwagger  =  require('hapi-swagger');

const swagger = {
    register: HapiSwagger,
    'options': {
        grouping: 'tags',
        payloadType: 'form',//form || json 
        info: {
            "title": "FindMate Admin API Documentation",
            //     "description": `
            //    JSDocs : http://34.212.218.2/sync-1-to-1-server/web/routes/out/index.html           
            //     `
        }
    }
}

module.exports = { inert, vision, swagger };