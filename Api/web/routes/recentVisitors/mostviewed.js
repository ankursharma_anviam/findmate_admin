const Joi = require("joi");
const logger = require('winston');
const userMatchCollection = require("../../../models/recentVisitors");


APIHandler = (req, res) => {
    let condition = [
      {  "$group":
        {
            "_id": "$targetUserId",
            "totalViewed": { "$sum": 1 },
            "timestamp": { "$last": "$createdTimestamp" }
        }
    },
        { "$sort": { "totalViewed": -1 } },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "mostViewd" } },
        { "$unwind": "$mostViewd" },
        {
            "$project": {
                "firstName": "$mostViewd.firstName", "gender": "$mostViewd.gender", "registerd": "$mostViewd.registeredTimestamp", "totalViewed": 1, "timestamp": 1, "profileVideo": "mostViewd.profileVideo", "city": "$mostViewd.address.city", "country": "$mostViewd.address.country",
                "contactNumber": "$mostViewd.contactNumber", "email": "$mostViewd.email", "profilePic": "$mostViewd.profilePic", "profileVideo": "$mostViewd.profileVideo", "dob": "$mostViewd.dob", "height": "$mostViewd.height", "mostViewd.country": 1, "lastLogin": "$mostViewd.lastLogin"
            }
        }
    ];

    userMatchCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: 'internal server error' }).code(500);
        } else if (result) {
            return res({ code: 200, message: 'success', data: result }).code(200);
        } else {
            return res({ message: 'incorrect password' }).code(204);
        }
    });
};

module.exports = { APIHandler }