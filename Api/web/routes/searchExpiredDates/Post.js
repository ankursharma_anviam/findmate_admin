
const Joi = require("joi");
const logger = require('winston');
const dateScheduleCollection = require('../../../models/dateSchedule');
const local = require("../../../locales");


const payloadValidator = Joi.object({

    text: Joi.string().description('_id'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 10;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [
        {
            "$project": {
                "DateRequestedBy": "$initiatorName",
                "initiatedBy": "$initiatedBy",
                "opponentId": "$opponentId",
                "OpponentId": "$opponentId",
                "DateRequestedById": "$initiatedBy",
                "DatedCreatedOn": "$createdTimestamp",
               //"DateType": "$negotiations.requestedFor",
                "DateSchedule": "$negotiations.proposedOn",
                "Opponent": "$opponentName",
                "ActivityTimeline": { "$size": "$negotiations" },
                "negotiations": { "$slice": ["$negotiations", -1] }, "createdTimestamp": 1, "dateFeedback": 1
            }
        },
        { "$unwind": "$negotiations" },
        {
            "$project": {
                "DateRequestedBy": 1,
                "initiatedBy": 1,
                "DateRequestedById": 1,
                "opponentId": 1,
                "OpponentId": 1,
                "DatedCreatedOn": 1,
                "DateType": "$negotiations.requestedFor",
                "DateSchedule": "$negotiations.proposedOn",
                "Opponent": 1,
                "ActivityTimeline": 1,
                "negotiations": 1, "createdTimestamp": 1, "dateFeedback": 1
            }
        },
        { "$sort": { "_id": -1 } },
        { "$sort": { "_id": -1 } },
        { "$lookup": { "from": "userList", "localField": "DateRequestedById", "foreignField": "_id", "as": "DataA" } },
        { "$unwind": "$DataA" },
        { "$lookup": { "from": "userList", "localField": "OpponentId", "foreignField": "_id", "as": "DataB" } },
        { "$unwind": "$DataB"},
        {
            "$project": {
                "DateRequestedBy": 1, "DatedCreatedOn": 1, "DateRequestedById": 1, "OpponentId": 1,
                "DateType": 1, "DateSchedule": 1, "Opponent": 1, "ActivityTimeline": 1,
                "negotiations": 1, "createdTimestamp": 1, "dateFeedback": 1,
                "DateRequestedEmail": "$DataA.email",
                "DateRequestedNumber": "$DataA.contactNumber",
                "OpponentEmail": "$DataB.email",
                "OpponentNumber": "$DataB.contactNumber",
            }
        }, { "$skip": offset * limit }, { "$limit": limit }
    ];

    /*
   add condition for status
   */
    switch (req.payload.status) {
        case "All": {
            //no add any condition for date status
            break;
        }
        case "Confirmed": {
            condition.push({ "$match": { "negotiations.opponentResponse": "accepted" } });
            break;
        }
        case "NotConfirmed": {
            condition.push({ "$match": { "negotiations.opponentResponse": "" } });
            break;
        }
        case "Completed": {
            condition.push({ "$match": { "$or": [{ "negotiations.opponentResponse": "accepted" }, { "negotiations.requestedFor": "Now" }] } });
            condition.push({ "$match": { "dateFeedback.rating": { "$exists": true } } });
            break;
        }
        case "Rejected": {
            condition.push({ "$match": { "negotiations.opponentResponse": "denied" } });
            break;
        }
        case "Expired": {
            condition.push({ "$match": { "negotiations.proposedOn": { "$lt": new Date().getTime() } } });
            condition.push({ "$match": { "negotiations.opponentResponse": "" } });
            condition.push({ "$match": { "dateFeedback.rating": { "$exists": false } } });
            break;
        }



    }

    /*
    add condition for rating
    */
    switch (req.payload.rating) {
        case "All": {
            //no add any condition for date status
            break;
        }
        case "1": {
            condition.push({ "$match": { "dateFeedback.rating": 1 } });
            break;
        }
        case "2": {
            condition.push({ "$match": { "dateFeedback.rating": 2 } });
            break;
        }
        case "3": {
            condition.push({ "$match": { "dateFeedback.rating": 3 } });
            break;
        }
        case "4": {
            condition.push({ "$match": { "dateFeedback.rating": 4 } });
            break;
        }
        case "5": {
            condition.push({ "$match": { "dateFeedback.rating": 5 } });
            break;
        }
    }

    /*
 add condition for date type
 */
    switch (req.payload.dateType) {
        case "All": {
            //no add any condition for date status
            break;
        }
        case "physicalDate": {
            condition.push({ "$match": { "negotiations.requestedFor": "physicalDate" } });
            break;
        }
        case "videoDate": {
            condition.push({ "$match": { "negotiations.requestedFor": "videoDate" } });
            break;
        }
        case "audioDate": {
            condition.push({ "$match": { "negotiations.requestedFor": "audioDate" } });
            break;
        }


    }

    /*
   add condition for from Date
   */
    if (req.payload.dateFrom) {
        // condition.push({ "$match": { "negotiations.proposedOn": { "$gte": req.payload.dateFrom } } });
        condition.push({ "$match": { "DatedCreatedOn": { "$gte": req.payload.dateFrom } } });

    }
    /*
   add condition for to Date
   */
    if (req.payload.dateTo) {
        condition.push({ "$match": { "DatedCreatedOn": { "$lte": req.payload.dateTo } } });
    }

    /*
   add condition for text
   */
    if (req.payload.text != "All" && req.payload.text != "") {
        condition.push({
            "$match": {
                "$or": [
                    { "DateRequestedBy": new RegExp(req.payload.text, 'i') },
                    { "Opponent": new RegExp(req.payload.text, 'i') },
                    { "DateRequestedEmail": new RegExp(req.payload.text, 'i') },
                    { "DateRequestedNumber": new RegExp(req.payload.text, 'i') },
                    { "OpponentEmail": new RegExp(req.payload.text, 'i') },
                    { "OpponentNumber": new RegExp(req.payload.text, 'i') },
                ]
            }
        });
    }

    console.log("Condition ", JSON.stringify(condition));
    dateScheduleCollection.Aggregate(condition, (err, result) => {

        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }

        let dataToSend = [];
        for (var index = 0; index < result.length; index++) {

            // if (result[index]["dateFeedback"].length) {
            //     result[index]["DateStatus"] = "Completed";

            //     if (result[index]["dateFeedback"][0] && result[index]["dateFeedback"][0]["reviewerId"] == result[index]["initiatedBy"]) {
            //         result[index]["RequestorRating"] = result[index]["dateFeedback"][0]["rating"];
            //     } else if (result[index]["dateFeedback"][1] && result[index]["dateFeedback"][1]["rating"]) {
            //         result[index]["RequestorRating"] = result[index]["dateFeedback"][1]["rating"];
            //     } else {
            //         result[index]["RequestorRating"] = "NA";
            //     }

            //     if (result[index]["dateFeedback"][0] && result[index]["dateFeedback"][0]["reviewerId"] == result[index]["opponentId"]) {
            //         result[index]["OpponentRating"] = result[index]["dateFeedback"][0]["rating"];
            //     } else if (result[index]["dateFeedback"][1] && result[index]["dateFeedback"][1]["rating"]) {
            //         result[index]["OpponentRating"] = result[index]["dateFeedback"][1]["rating"];
            //     } else {
            //         result[index]["OpponentRating"] = "NA";
            //     }

            //     dataToSend.push(result[index]);

            // } 
            // else if (!result[index]["dateFeedback"].length
            //     && result[index]["negotiations"]["opponentResponse"] == "accepted"
            //     && result[index]["negotiations"]["proposedOn"] > new Date().getTime()) {

            //     result[index]["DateStatus"] = "Confirmed";

            // } else if (!result[index]["dateFeedback"].length
            //     && result[index]["negotiations"]["opponentResponse"] == ""
            //     && result[index]["negotiations"]["proposedOn"] > new Date().getTime()) {

            //     result[index]["DateStatus"] = "Not Confirmed";

            // } else 
            if (!result[index]["dateFeedback"].length
                && result[index]["negotiations"]["proposedOn"] < new Date().getTime()) {

                result[index]["DateStatus"] = "Expired";
                result[index]["ExpiredOn"] = result[index]["negotiations"]["proposedOn"];
                dataToSend.push(result[index]);
            }

            // else if (!result[index]["dateFeedback"].length
            //     && result[index]["negotiations"]["opponentResponse"] == "denied") {

            //     result[index]["DateStatus"] = "Rejected";
            //     result[index]["OpponentRating"] = "NA";
            //     result[index]["RequestorRating"] = "NA";
            //     dataToSend.push(result[index]);
            // }

        }
        dateScheduleCollection.TotalCount({}, (err, ress) => {
            return res({ message: req.i18n.__('PostExpiredDates')['200'], data: result, totalCount: ress }).code(200);
        });
        //return res({ message: req.i18n.__('PostExpiredDates')['200'], data: dataToSend }).code(200);
    })
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostExpiredDates']['200']), data: Joi.any(), index: Joi.any(),totalCount:Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }