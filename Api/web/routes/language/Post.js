const Joi = require("joi");
const logger = require('winston');
const languagesCollection = require('../../../models/languages');
const local  = require("../../../locales");


const payloadValidator = Joi.object({
    languageName: Joi.string().required().description('languageName'),
    RTL: Joi.string().required().description('RTL'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {


    var data = {
        "languageName": req.payload.languageName,
        "RTL": req.payload.RTL
    };
    languagesCollection.Insert(data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }else {
            return res({ message: req.i18n.__('PostLanguage')['200'] }).code(200);
        }

    })

};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostLanguage']['200']),
        },
        422: { message: Joi.any().default(local['PostLanguage']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }