
let headerValidator = require('../../middleware/validator');
let PostAPI = require("./Post");

module.exports = [
   
    {
        method: 'POST',
        path: '/language',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'language'],
            description: 'This API is used to Post Language.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },
   
];