'use strict';
const Joi = require("joi");
const logger = require('winston');
const appRate = require("../../../models/appRate");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [{ "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
    { "$unwind": "$Data" },
    { "$project": { "userId": 1, "Rating": "$rate", "Review": "$message", "RatedBy": "$Data.firstName", "Date": "$creation" } },
    { "$sort": { "_id": -1 } },
    { "$skip": offset },
    { "$limit": limit },
    ];

    appRate.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            for (var index = 0; index < result.length; index++) {
                if (result[index] && !result[index].message) {
                    result[index].message = "N/A";
                }
            }
            return res({ message: req.i18n.__('GetAppRate')['200'], data: result }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetAppRate']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetAppRate']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }