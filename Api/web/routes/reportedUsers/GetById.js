'use strict';

const Joi = require("joi");
const logger = require('winston');
const userReports = require("../../../models/userReports");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const APIHandler = (req, res) => {

    let _id = req.params.userId;

    let condition = [
        { "$match": { "targetUserId": ObjectID(_id) } },
        { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "creation": 1, "ReportedBy": "$Data.firstName", "reason": 1,
                "dob": "$Data.dob",
                "profilePic": "$Data.profilePic",
                "email": "$Data.email",
                "gender": "$Data.gender",
                "height": "$Data.height",
                "heightInFeet": "$Data.heightInFeet",
                "city": "$Data.address.city",
                "country": "$Data.address.country",
                "contactNumber": "$Data.contactNumber",
                "profileVideo": "$Data.profileVideo",
                "otherImages": "$Data.otherImages",
                "myPreferences": "$Data.myPreferences"

            }
        },
        { "$sort": { "creation": -1 } }
    ];

    userReports.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetByIdReportedUser')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetByIdReportedUser')['204'] }).code(204);
        }
    });
};

const payloadValidator = Joi.object({
    userId: Joi.string().required().description('iserId'),

}).options({ allowUnknown: true });

const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['GetByIdReportedUser']['200']), data: Joi.any()
    //     },
    //     204: { message: Joi.any().default(local['GetByIdReportedUser']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}
module.exports = { APIHandler, payloadValidator, response }