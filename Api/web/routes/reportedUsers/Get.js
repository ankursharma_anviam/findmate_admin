'use strict';

const Joi = require("joi");
const logger = require('winston');
const userReports = require("../../../models/userReports");
const local  = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [
        {
            "$group": {
                _id: "$targetUserId",
                "creation": { "$last": "$creation" },
                "NoOfReports": { "$sum": 1 }
            }
        },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        { "$project": { "creation": 1, "ReportedUser": "$Data.firstName", "NoOfReports": 1,
         "PhoneNo": "$Data.contactNumber" } },
        { "$sort": { "creation": -1 } },
        { "$skip": offset },
        { "$limit": limit },
    ];


    userReports.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message:  req.i18n.__('GetReportedUser')['200'], data: result }).code(200);
        } else {
            return res({ message:  req.i18n.__('GetReportedUser')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetReportedUser']['200']),data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetReportedUser']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler,response}