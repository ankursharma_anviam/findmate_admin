
let headerValidator = require('../../middleware/validator');
let GetByIdAPI = require('./GetById');

module.exports = [
   
    {
        method: 'GET',
        path: '/blockedUsers/{userId}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'user'],
            description: 'This API is used to get Blocked Report user data by Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response
        }
    }
];