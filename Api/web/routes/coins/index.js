let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let PutAPI = require('./Put');
let DeleteAPI=require('./Delete');
let GetByText = require('./GetByText');
module.exports = [
    {
        method: 'GET',
        path: '/coin',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'coins'],
            description: 'This API is used to Get coins.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
   
    {
        method: 'POST',
        path: '/coin',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'coins'],
            description: 'This API is used to Upload coin.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/coin',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'coins'],
            description: 'This API is used to update coin.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'POST',
        path: '/coins',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'coins'],
            description: 'This API is used to Delete Id wise coin.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },

    {
        method: 'GET',
        path: '/coin/{text}',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'coins'],
            description: 'This API is used to search coin By coinTitle.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    }

];