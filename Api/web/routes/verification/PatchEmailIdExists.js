'use strict';

const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const allMessage = require('../../../language.json');
const Auth = require("../../middleware/authentication.js");
const userListCollection = require('../../../models/userList');
const local  = require("../../../locales");

let payloadValidator = Joi.object({
    email: Joi.string().required().description("enter emailID").example("example@app.com").error(new Error('email is missing')),
}).required();

/**
 * @method PATCH verificationEmailidExists
 * @description This API use to PATCH verificationEmailidExists.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} email email
 
 * @returns  200 : Email-id exist in the database.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Email-id doesnot exist in the database.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {


    let email = req.payload.email;
    let condition = { email: email };

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500']}).code(500);
        } else if (result[0]) {
            return res({ message: " req.i18n.__('PatchEmailIdExists')['200'] not registerd" }).code(200);
        } else {
            return res({ message:" req.i18n.__('PatchEmailIdExists')['412'] regostered" }).code(412);
        }
    });

};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PatchEmailIdExists']['200']),
        },
        412: { message: Joi.any().default(local['PatchEmailIdExists']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { APIHandler, payloadValidator,response }