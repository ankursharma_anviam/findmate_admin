'use strict';
const Joi = require("joi");
const logger = require('winston');
var userListCollection = require("../../../models/userList");
const local = require("../../../locales");


const payloadValidator = Joi.object({
    userType: Joi.string().required().allow(["All", "paid", "normal"]).description('Configuration change userType'),
    type: Joi.string().required().allow(["All", "View", "Match", "Likes", "OnlineDate"]).description('Configuration change sort '),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {
    let condition = [];
    /*
        add condition for paid and normal userType
    */
    switch (req.payload.userType) {
        case "All": {
            break;
        }
        case "paid": {
            condition.push({
                "$match": { "userType": { "$in": ["paid"] } }
            });
            break;
        }
        case "normal": {
            condition.push({
                "$match": { "userType": { "$in": ["normal"] } }
            });
            break;
        }
    }

    /*
               add condition for lke and match userType
           */

    switch (req.payload.type) {
        case "Likes": {
            condition.push(
                {
                    "$project": {
                        firstName: 1,
                        likedBy: 1,
                        userType: 1,
                        MyLikeCount: { "$size": { "$ifNull": ["$likedBy", []] } },

                    }

                },
                { "$sort": { "MyLikeCount": -1 } }
            );
            break;
        }
        case "Match": {
            condition.push(
                {
                    "$project": {
                        firstName: 1,
                        likedBy: 1,
                        userType: 1,
                        matchedWith: { "$size": { "$ifNull": ["$friendShipId", []] } },

                    }

                },
                { "$sort": { "matchedWith": -1 } }
            );

            break;
        }
        case "View": {
            condition.push(
                {
                    "$project": {
                        firstName: 1,
                        likedBy: 1,
                        userType: 1,
                        recentVisitors: { "$size": { "$ifNull": ["$recentVisitors", []] } },

                    }

                },
                { "$sort": { "recentVisitors": -1 } }
            );
            break;
        }
    }


    console.log("Condirion----------------->>>>>>>>", JSON.stringify(condition));
    userListCollection.Aggregate(condition, (err, result) => {
        if (err) {
            console.log("===>>>", err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            // result.push({ "$match": { "gender": { "$in": ["Male"] } } })
            return res({ message: req.i18n.__('PostFilter')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('PostFilter')['422'] }).code(422);
        }
    })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostFilter']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['PostFilter']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }