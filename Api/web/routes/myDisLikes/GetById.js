'use strict';

const Joi = require("joi");
const logger = require('winston');
const userUnlikes = require("../../../models/userUnlikes");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {


    let condition = [
        { "$match": { "userId": ObjectID(req.params._id), "isMatch": { "$exists": false } } },
        { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "UnlikeOn": "$timestamp", "Unlike": "$Data.firstName",
                "PhoneNo": "$Data.contactNumber",
                "Email": "$Data.email",
                "dob": "$Data.dob",
                "profilePic": "$Data.profilePic",
                "gender": "$Data.gender",
                "height": "$Data.height",
                "profileVideo": "$Data.profileVideo",
                "heightInFeet": "$Data.heightInFeet",
                "city": "$Data.address.city",
                "country": "$Data.address.country",
                "otherImages": "$Data.otherImages",
                "myPreferences": "$Data.myPreferences",
                "about": "$Data.about"

            }
        },
        { "$sort": { "UnlikeOn": -1 } }

    ]
    userUnlikes.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        result.forEach(element => {
            if (element.Email !== null && element.Email !== undefined && element.Email !== '') {
                var maskid = "";
                var myemailId = element.Email;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.Email = maskid;
            }
            if (element.PhoneNo !== null && element.PhoneNo !== undefined && element.PhoneNo !== '') {
                var maskid = "";
                var myemailId = element.PhoneNo;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.PhoneNo = maskid;
            }

        });

        return res({ message: req.i18n.__('GetByIdMyDisLike')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetByIdMyDisLike']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetByIdMyDisLike']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }