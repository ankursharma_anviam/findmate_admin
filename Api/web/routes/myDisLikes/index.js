
let headerValidator = require('../../middleware/validator');
let GetByIdAPI = require('./GetById');

module.exports = [
  
    {
        method: 'GET',
        path: '/myDisLikes/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'likes'],
            description: 'This API is used to Get My Dislikes.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response

        }
    }
];