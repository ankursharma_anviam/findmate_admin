
'use strict';

const joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const utils = require('./fileUploader');
const errorMsg = require("../../../locales");


const payload = {
    maxBytes: 1000 * 1000 * 10, // 10 Mb
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data'
};//payload of upload image api

const payloadValidator = {
    photo: joi.any().meta({ swaggerType: 'file' }).description('Video to upload').error((new Error('Video is missing')))
}

/**
 * @function POST /uploadVideo
 * @description -New video Upload
 * @param {*} req 
 * @param {*} reply 
 */

let handler = (req, reply) => {


    const checkProvider = function () {
        return new Promise((resolve, reject) => {
            const data = req.payload;
            const file = data['photo'];

            let filePath = "/var/www/html/datum_2.0-admin/videoFile";

            if (typeof req.payload.type != 'undefined')
                filePath += (req.payload.type).toLowerCase();

            const fileOptions = { dest: `${filePath}/`, baseUrl: "/var/www/html/datum_2.0-admin/videoFile" };

            utils.uploader(file, fileOptions)
                .then((result) => {
                    return resolve(result);
                }).catch((err) => {
                    return reject({ message: req.i18n.__('VideoUploade')['204'] })
                });
        });
    }

    checkProvider()
        .then(data => {
            // console.log(" process.env", process.env);
            return reply({ message: req.i18n.__('VideoUploade')['200'], videopath: 'https://xxxxxxxxxxxxxxxx/datum_2.0-admin/videoFile/' + data.fileName });
        }).catch(e => {
            logger.error("error while uploading image =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: joi.any().default(errorMsg['VideoUploade']['200']), videopath: joi.any() },
        204: { message: joi.any().default(errorMsg['VideoUploade']['204']) },
        500: { message: joi.any().default(errorMsg['genericErrMsg']['500']) }
    }
}//swagger response code



module.exports = {
    payload,
    payloadValidator,
    handler,
    responseCode
};