let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/liksBy',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'likes'],
            description: 'This API is used to Get All Likes.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/liksBy/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'likes'],
            description: 'This API is used to Get Like By Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response,

        }
    }
];