
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PutAPI = require('./Put');
let PutBanAPI = require('./PutAll');


module.exports = [
    {
        method: 'GET',
        path: '/bannedUsers',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'bannedUsers'],
            description: 'This API is used to  get all Ban.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'Put',
        path: '/bannedUsers',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'bannedUsers'],
            description: 'This API is used to Bann a User from admin.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload : PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Put',
        path: '/bannedAllUsers',
        handler: PutBanAPI.APIHandler,
        config: {
            tags: ['api', 'bannedUsers'],
            description: 'This API is used to ban multipale user.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload : PutBanAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutBanAPI.response
        }
    }
];