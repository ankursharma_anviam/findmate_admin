
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI=require('./Put');

module.exports = [
    {
        method: 'GET',
        path: '/deactiveUsers',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'user'],
            description: 'This API is used to Get deactive users.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'PUT',
        path: '/deactive',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'user'],
            description: 'This API is used to user DeActive Users.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    }
];