'use strict'
const Joi = require("joi");
const logger = require('winston');
const userList = require("../../../models/userList");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {


    const condition = { "isDeactive": true }
    const project = { firstName:1, profilePic:1 }



    userList.selectWithProject(condition, { "_id": -1 }, project, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }

        return res({ message: req.i18n.__('GetDeactiveUser')['200'], data: result }).code(200);
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetDeactiveUser']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetDeactiveUser']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }