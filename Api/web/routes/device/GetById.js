'use strict'
const Joi = require("joi");
const logger = require('winston');
const userDevices = require("../../../models/userDevices");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {

    let condition = [
        { "$match": { "userId": ObjectID(req.params._id) } },
    
        {
            "$project": {
                userId: 1,
                deviceId: 1,
                deviceMake: 1,
                deviceModel: 1,
                deviceType: 1,
                deviceOs: 1,
                appVersion: 1,
                DevicetypeMsg:1,
                "lastOnline":"$creationDate"
            }
        },

    
        { "$sort": { "_id": -1 } }
    ]
    userDevices.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        
        return res({ message: req.i18n.__('GetDevice')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetDevice']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetDevice']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }