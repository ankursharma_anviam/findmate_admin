
let headerValidator = require('../../middleware/validator');
let GetByIdAPI = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/device/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'device'],
            description: 'This API is used to Get users Divice By Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response,

        }
    }
];