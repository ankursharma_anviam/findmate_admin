'use strict'
const Joi = require("joi");
const logger = require('winston');
const dollarToCoin = require("../../../models/dollarToCoin");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    dollarToCoin.Select({}, limit, offset * limit, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            dollarToCoin.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('dollarGet')['200'], data: result, totalCount: ress }).code(200);
            });
        } else {
            return res({ message: req.i18n.__('dollarGet')['204'] }).code(204);
        }
    });
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['dollarGet']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['dollarGet']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, queryValidator }