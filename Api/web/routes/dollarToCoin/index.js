let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
// let PostAPI = require('./Post');
// let PutAPI = require('./Put');
// let DeleteAPI=require('./Delete');
// let GetByText = require('./GetByText');
module.exports = [
    {
        method: 'GET',
        path: '/dollartoCoin',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'dollartoCooin'],
            description: 'This API is used to Get dollartoCooin.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
   
    // {
    //     method: 'POST',
    //     path: '/dollartoCooin',
    //     handler: PostAPI.APIHandler,
    //     config: {
    //         tags: ['api', 'coins'],
    //         description: 'This API is used to Upload coin.',
    //         auth: "adminJwt",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             payload: PostAPI.payloadValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: PostAPI.response

    //     }
    // },
    // {
    //     method: 'PUT',
    //     path: '/dollartoCooin',
    //     handler: PutAPI.APIHandler,
    //     config: {
    //         tags: ['api', 'coins'],
    //         description: 'This API is used to update coin.',
    //         auth: "adminJwt",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             payload: PutAPI.payloadValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: PutAPI.response
    //     }
    // },
 
    
];