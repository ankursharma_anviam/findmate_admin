'use strict'
const Joi = require("joi");
const logger = require('winston');
const coin = require('../../../models/coins');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const payloadValidator = Joi.object({
    coinId: Joi.array().required().description('coinIds'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let array = [];
    req.payload.coinId.forEach(element => {
        array.push(new ObjectID(element));
    });
    let condition = {
        '_id': { '$in': array }
    };
    let datatoUpdate = {
        statusCode: 0,
        status: "deleted",
        deleteTimestamp: new Date().getTime(),
    };


    coin.Delete(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('DeletePlans')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['Deletecoins']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }