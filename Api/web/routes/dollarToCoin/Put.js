'use strict'
const Joi = require("joi");
const logger = require('winston');
const coin = require('../../../models/coins');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('Id'),
    coinTitle: Joi.string().description('coinTitle'),
    imgURL: Joi.string().description('coinTitle'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {

    let data = {

        "coinTitle": req.payload.coinTitle,
        "imgURL": req.payload.imgURL,
    };

    coin.UpdateById(ObjectID(req.payload._id), data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('Putcoins')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['Putcoins']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }