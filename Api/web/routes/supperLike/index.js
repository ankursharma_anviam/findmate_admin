let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');
let GetMyByIdAPI = require('./GetByIdMy');

module.exports = [
    {
        method: 'GET',
        path: '/supperliksBy',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'supperLikes'],
            description: 'This API is used to Get Supperlikes.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'GET',
        path: '/supperliksBy/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'supperLikes'],
            description: 'This API is used to Get my SupperLikers.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response

        }
    },
    {
        method: 'GET',
        path: '/mysupperliksBy/{_id}',
        handler: GetMyByIdAPI.APIHandler,
        config: {
            tags: ['api', 'supperLikes'],
            description: 'This API is used to Get my SupperLikers.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetMyByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyByIdAPI.response

        }
    }
];