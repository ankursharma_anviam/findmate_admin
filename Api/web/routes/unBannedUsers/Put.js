'use strict';
const Joi = require("joi");
const logger = require('winston');
const userListCollection = require("../../../models/userList");
const userTypeCollection = require("../../../models/userListType");

const sendMail = require("../../../library/mailgun");
const mqtt = require("../../../library/mqtt");
const local = require("../../../locales");


const APIHandler = (req, res) => {

    let datatoUpdate = {
        profileStatus: 0,
        banndTimeStamp: 0,
        banndReson: ""
    };
    userTypeCollection.Update(req.payload._id, datatoUpdate, (err, result) => { if (err) logger.error(err) })

    userListCollection.UpdateById(req.payload._id, datatoUpdate, (err, result) => {

        if (err) {
            return res({ message: req.i18n.__('PutUnBanned')['422'] }).code(422);
        }
        return res({ message: req.i18n.__('PutUnBanned')['200'] }).code(200);

    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('_id'),
}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutUnBanned']['200']),
        },
        422: { message: Joi.any().default(local['PutUnBanned']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { APIHandler, payloadValidator, response }