const Joi = require("joi");
const fs = require('fs');
var moment = require('moment');


const payloadValidator = Joi.object({
    activeimage: Joi.any().required().description('img'),
}).options({ allowUnknown: true });
/**
* function to decode base 64 
*/
function base64_decode(data) {
    // console.log("data"+data);
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
        ac = 0,
        dec = '',
        tmp_arr = [];
    if (!data) {
        return data;
    }

    data += '';
    do { // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));
        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;
        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    dec = tmp_arr.join('');
    return dec.replace(/\0+$/, '');
}
const APIHandler = (req, res) => {

    var fileData = base64_decode(req.payload.activeimage.split(',')[1]);
    var ImageName = moment().valueOf() + ".png";
    //var responseObj = {};
    var target_path = `/var/www/html/datum_2.0-admin/imageFile/` + ImageName;

    fs.appendFile(target_path, fileData, 'binary', function (ress) {
        if (ress) {
            return res({ message: 'upload success   ' }).code(500);
        } else {
            var activeUrl =`http://xxxxxxxxxxx/datum_2.0-admin/imageFile/` + ImageName
            return res({ message: 'upload failed', res: activeUrl }).code(200);

        }
    });



};

module.exports = { APIHandler, payloadValidator }