'use strict';

const Joi = require("joi");
const logger = require('winston');
const dateSchedule = require("../../../models/dateSchedule");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const APIHandler = (req, res) => {

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let _id = req.params._id;
    let condition = [
        { "$match": { "_id": ObjectID(_id) } },
        { "$unwind": "$negotiations" },
        { "$project": { "negotiations": 1 } },
        { "$lookup": { "from": "userList", "localField": "negotiations.proposerId", "foreignField": "_id", "as": "requestorData" } },
        { "$unwind": "$requestorData" },
        { "$lookup": { "from": "userList", "localField": "negotiations.opponnedId", "foreignField": "_id", "as": "opponnedData" } },
        { "$unwind": "$opponnedData" },
        {
            "$project": {
                "RequestorId": "$requestorData._id",
                "OpponentId": "$opponnedData._id",
                "Requestor": "$requestorData.firstName",
                "RequestedOn": "$negotiations.proposedTime",
                "RequestedFor": "$negotiations.proposedOn",
                "RequestType": "$negotiations.opponentResponse",
                "Opponent": "$opponnedData.firstName",
                "OpponentResponse": "$negotiations.opponentResponse",
                "OpponentResponseTimestamp": "$negotiations.proposedTime",
                "placeName":"$negotiations.placeName"

            }
        },
        { "$skip": offset },
        { "$limit": limit },
    ]

    dateSchedule.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }

        for (var index = 0; index < result.length; index++) {

            if (index == 0) {
                result[index]["RequestType"] = "New Date";
                if (result[index]["OpponentResponse"] == "") {
                    result[index]["OpponentResponse"] = "Expired";
                }
            } else {
                result[index]["RequestType"] = result[index - 1]["OpponentResponse"] || "Expired";
                if (result[index]["OpponentResponse"] == "") {
                    result[index]["OpponentResponse"] = "Expired";
                }
            }

        }

        return res({ message: req.i18n.__('GetCompletedDates')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetCompletedDates']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetCompletedDates']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }