
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByText = require('./GetByText');


module.exports = [

    {
        method: 'GET',
        path: '/walletApp',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'walletApp'],
            description: 'This API is used to Get User All walletApp.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.payloadValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
   
    {
        method: 'GET',
        path: '/walletAppByText',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'walletApp'],
            description: 'This API is used to search walletApp By txnType,paymentType.initatedBy.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },

   
];