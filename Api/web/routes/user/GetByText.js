"use strict";
const Joi = require("joi");
const logger = require('winston');
const userList = require("../../../models/userList");
const local  = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().required().description('enter You want to search'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {



    let condition = {};
    var user = req.params.text.trim();
    if (user == "") {
        condition = {  "isDeactive": { "$exists": false }, "isDeactive": false };
    } else {

        condition =

            {
                $or:[
                    {"isDeactive": { "$exists": false }},
                    {"isDeactive": false}
                ]
                ,
                
                "$or": [
                    { "firstName": new RegExp(user, "i") },
                    { "contactNumber": new RegExp(user, "i") },
                    { "email": new RegExp(user, "i") }
                ],


            }
        console.log("======>>>", condition);
    }
    userList.Select(condition, (err, result) => {
        if (err) {
            return res({ message:req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message:req.i18n.__('GetBytxt')['200'], data: result}).code(200);
        } else {
            return res({ message:req.i18n.__('GetBytxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetBytxt']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetBytxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = {APIHandler,payloadValidator,response}