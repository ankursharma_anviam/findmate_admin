'use strict'
const Joi = require("joi");
const logger = require('winston');
const userList = require("../../../models/userList");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    userList.Aggregate([
        { "$match": { "profileStatus": { $ne: 1 } } },
        {
            "$project": {
                firstName: 1, email: 1, contactNumber: 1, registeredTimestamp: 1, lastOnlineStatus: 1, city: "$address.city", country: "$address.country", "dob": 1, "gender": 1, "height": 1, "heightInFeet": 1, "profilePic": 1, "profileVideo": 1, "registeredTimestamp": 1,
                myLikes: { "$size": { "$ifNull": ["$myLikes", []] } },
                likedBy: { "$size": { "$ifNull": ["$likedBy", []] } },
                myunlikes: { "$size": { "$ifNull": ["$myunlikes", []] } },
                mySupperLike: { "$size": { "$ifNull": ["$mySupperLike", []] } },
                supperLikeBy: { "$size": { "$ifNull": ["$supperLikeBy", []] } },
                disLikedUSers: { "$size": { "$ifNull": ["$disLikedUSers", []] } },
                matchedWith: { "$size": { "$ifNull": ["$matchedWith", []] } },
                recentVisitors: { "$size": { "$ifNull": ["$recentVisitors", []] } }

            }
        },

        { "$sort": { "_id": -1 } },



        { "$skip": offset },
        { "$limit": limit },
    ],
        (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else if (result) {
                for (let index = 0; index < result.length; index++) {
                    result[index]["lastOnlineStatus"] = result[index]["lastOnlineStatus"] || result[index]["registeredTimestamp"]
                    delete result[index]["registeredTimestamp"];
                }

                return res({ message: req.i18n.__('GetAllUser')['200'], data: result }).code(200);
            } else {
                return res({ message: req.i18n.__('GetAllUser')['204'] }).code(204);
            }
        });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetAllUser']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetAllUser']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }