
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PutAPI = require("./Put");
module.exports = [
    {
        method: 'GET',
        path: '/communityGuidelines',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'Community Guidelines'],
            description: 'This API is used to get Community Guidelines.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/communityGuidelines',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'Community Guidelines'],
            description: 'This API is used to update Community Guidelines.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response

        }
    }
];