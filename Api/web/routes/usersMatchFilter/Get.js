'use strict';
const Joi = require("joi");
const moment = require('moment');
const logger = require('winston');
const userMatchCollection = require("../../../models/userMatch");
const local = require("../../../locales");


const payloadValidator = Joi.object({
    dateFrom: Joi.number().default().description('Enter From Date(milliseconds) For Ex.946684800000'),
    dateTo: Joi.number().default().description('Enter To Date(milliseconds) for Ex. 33765897600000'),
    text: Joi.string().description('enter You want to search'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let date = parseInt(req.query.dateTo)
    let dateTo = moment(date).add(24, 'hours').valueOf()
    let condition = [


        { '$lookup': { "from": "userList", "localField": "initiatedBy", "foreignField": "_id", "as": "userData" } },
        { "$lookup": { "from": "userList", "localField": "opponentId", "foreignField": "_id", "as": "targetUserData" } },
        { "$unwind": "$userData" },
        { "$match": { "userData.profileStatus": { "$ne": 1 } } },
        { "$unwind": "$targetUserData" },

        {
            "$project": {
                "matchdBy": "$userData.firstName",
                "matchByEmail": "$userData.email",
                "matchByheight": "$userData.height",
                "matchByheightInFeet": "$userData.heightInFeet",
                "matchBycity": "$userData.address.city",
                "matchBycountry": "$userData.address.country",
                "matchByContactNumber": "$userData.contactNumber",
                "matchByprofilePic": "$userData.profilePic",
                "matchByMyPreferences": "$userData.myPreferences",
                "matchByabout": "$userData.about",
                "matchByprofileVideo": "$userData.profileVideo",
                "matchByOtherImages": "$userData.otherImages",
                "matchBygender": "$userData.gender",
                "matchBydob": "$userData.dob",
                "opponentUser": "$targetUserData.firstName",
                "opponentUserEmail": "$targetUserData.email",
                "opponentUserContactNumber": "$targetUserData.contactNumber",
                "opponentUserheight": "$targetUserData.height",
                "opponentUserprofilePic": "$targetUserData.profilePic",
                "opponentUserheightInFeet": "$targetUserData.heightInFeet",
                "opponentUserBycity": "$targetUserData.address.city",
                "opponentUserByabout": "$targetUserData.about",
                "opponentUsercountry": "$targetUserData.address.country",
                "opponentUsergender": "$targetUserData.gender",
                "opponentUserdob": "$targetUserData.dob",
                "opponentByOtherImages": "$targetUserData.otherImages",
                "opponentUserprofileVideo": "$targetUserData.profileVideo",
                "opponentUserMyPreferences": "$targetUserData.myPreferences",
                "matchOn": "$createdTimestamp"
            }
        },
        { "$sort": { "matchOn": -1 } }

    ];

    if (req.query.dateFrom) {
        condition.push({ "$match": { "matchOn": { "$gte": parseInt(req.query.dateFrom) } } });

    }
    /*
   add condition for to Date
   */
    if (dateTo) {
        condition.push({ "$match": { "matchOn": { "$lte": dateTo } } });
    }
    /*
   add condition for text search
   */

    if (req.query.text != "") {
        condition.push(

            {
                "$match": {
                    "$or": [
                        { "matchdBy": new RegExp(req.query.text, 'i') },
                        { "matchByEmail": new RegExp(req.query.text, 'i') },
                        { "matchByContactNumber": new RegExp(req.query.text, 'i') },
                        { "opponentUser": new RegExp(req.query.text, 'i') },
                        { "opponentUserEmail": new RegExp(req.query.text, 'i') },
                        { "opponentUserContactNumber": new RegExp(req.query.text, 'i') }


                    ]
                }

            },
            { "$skip": offset * limit }, { "$limit": limit }
        );


    }
    else {
        condition.push({ "$skip": offset * limit }, { "$limit": limit });
    }

    //console.log("Condirion----------------->>>>>>>>", JSON.stringify(condition));
    userMatchCollection.Aggregate(condition, (err, result) => {
        if (err) {
            console.log("===>>>", err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                if (element.matchByEmail !== null && element.matchByEmail !== undefined && element.matchByEmail !== '') {
                    var maskid = "";
                    var myemailId = element.matchByEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.matchByEmail = maskid;
                }
                if (element.matchByContactNumber !== null && element.matchByContactNumber !== undefined && element.matchByContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.matchByContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.matchByContactNumber = maskid;
                }
                if (element.opponentUserEmail !== null && element.opponentUserEmail !== undefined && element.opponentUserEmail !== '') {
                    var maskid = "";
                    var myemailId = element.opponentUserEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.opponentUserEmail = maskid;
                }
                if (element.opponentUserContactNumber !== null && element.opponentUserContactNumber !== undefined && element.opponentUserContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.opponentUserContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.opponentUserContactNumber = maskid;
                }

            });

            userMatchCollection.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetUserMatchFilter')['200'], data: result, totalCount: ress }).code(200);
            });
            //return res({ message: req.i18n.__('GetUserMatchFilter')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetUserMatchFilter')['204'] }).code(204);
        }
    })
}
const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['GetUserMatchFilter']['200']), data: Joi.any()
    //     },
    //     204: { message: Joi.any().default(local['GetUserMatchFilter']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}

module.exports = { APIHandler, payloadValidator, response }