let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
module.exports = [
    
    {
        method: 'GET',
        path: '/userActivityFilters',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'usersFilter'],
            description: 'This API is use to  get From to To Date  vise like and Unlike  User by selecting user type .',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    }

];