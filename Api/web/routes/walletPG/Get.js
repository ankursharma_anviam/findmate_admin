'use strict'
const Joi = require("joi");
const logger = require('winston');
const walletPG = require("../../../models/walletPG");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const APIHandler = (req, res) => {

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    walletPG.selectWithProject({}, { "_id": -1 }, {}, offset*limit, limit, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(e => {
                e.opeingBalance = parseFloat(e.opeingBalance).toFixed(2)
                e.closingBalance = parseFloat(e.closingBalance).toFixed(2)

            });
            walletPG.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetWalletPG')['200'], data: result, totalCount: ress }).code(200);
            });
        } else {
            return res({ message: req.i18n.__('GetWalletPG')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetWalletPG']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetWalletPG']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, queryValidator }