
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByText = require('./GetByText');


module.exports = [

    {
        method: 'GET',
        path: '/walletPG',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'walletPG'],
            description: 'This API is used to Get User All walletPG.',
            auth: "adminJwt",
            validate: {
                query: GetAPI.queryValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },

    {
        method: 'GET',
        path: '/walletPGByText',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'walletPG'],
            description: 'This API is used to search walletPG By txnType,paymentType.initatedBy',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },


];