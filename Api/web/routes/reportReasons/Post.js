'use strict';

const Joi = require("joi");
const logger = require('winston');
const reportReasonsCollection = require('../../../models/reportReasons');
const local  = require("../../../locales");



const payloadValidator = Joi.object({
    reason: Joi.string().required().description('reason'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {


    let data = {
        "reason": req.payload.reason
    };

    reportReasonsCollection.Insert(data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message:req.i18n.__('PostReportReasonse')['200']}).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostReportReasonse']['200']), 
        },
        422: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }