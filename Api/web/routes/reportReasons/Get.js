'use strict';

const Joi = require("joi");
const logger = require('winston');
const reportReasonsCollection = require("../../../models/reportReasons");
const local = require("../../../locales");

const queryValidator = Joi.object({
}).required();

const APIHandler = (req, res) => {

    reportReasonsCollection.Select({}, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetReportReasonse')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetReportReasonse')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetReportReasonse']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetReportReasonse']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, response }