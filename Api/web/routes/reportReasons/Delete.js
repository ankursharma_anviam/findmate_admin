'use strict';

const Joi = require("joi");
const logger = require('winston');
const reportReasonsCollection = require('../../../models/reportReasons');
const ObjectID = require('mongodb').ObjectID;
const local  = require("../../../locales");


const payloadValidator = Joi.object({
    reasonId: Joi.string().required().description('reasonId'),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {

    let data = {
         _id : ObjectID(req.params.reasonId)
    };
    reportReasonsCollection.Delete(data, (err, result) => {
        if (err) {
            return res({ message:req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({message: req.i18n.__('DeleteReportReasonse')['200']}).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['DeleteReportReasonse']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }