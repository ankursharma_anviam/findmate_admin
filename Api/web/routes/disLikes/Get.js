'use strict';

const Joi = require("joi");
const logger = require('winston');
const userUnlikesCollection = require("../../../models/userUnlikes");
const local  = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [
        { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "userData" } },
        { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "targetUserData" } },
        { "$unwind": "$userData" },
        { "$unwind": "$targetUserData" },
        {
            "$project": {
                "LikedBy": "$userData.firstName",
                "Email": { "$concat": ["$userData.email", " / ", "$userData.contactNumber"] },
                "OpponentUser": "$targetUserData.firstName",
                "EmailTarget": { "$concat": ["$targetUserData.email", " / ", "$targetUserData.contactNumber"] },
                "LikedOn": "$timestamp"
            }
        },
        { "$sort": { "_id": -1 } },
        { "$skip": offset },
        { "$limit": limit },
    ];


    userUnlikesCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message:req.i18n.__('GetDisLike')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetDisLike')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetDisLike']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetDisLike']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler,response }