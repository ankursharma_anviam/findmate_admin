
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/disLikes',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'likes'],
            description: 'This API is used to Get All DisLikes Users.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/disLikes/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'likes'],
            description: 'This API is used to Get DisLikes By Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response,

        }
    }
];