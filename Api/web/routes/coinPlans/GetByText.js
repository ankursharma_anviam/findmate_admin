"use strict";
const Joi = require("joi");
const logger = require('winston');
const coinPlans = require("../../../models/coinPlans");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().description('search Coinplan by planName'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var user = req.params.text.trim();
    let condition = {offset,limit};
    var user = req.params.text.trim();
    if (user == "") {
    } else {

        condition =

            {
                "$or": [
                    { "planName": new RegExp(user, "i") },

                ],


            }
        console.log("======>>>", condition);
    }


    coinPlans.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetcoinplanByTxt')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetcoinplanByTxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinplanByTxt']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetcoinplanByTxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }