
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdText = require('./GetByTextId');
let GetById = require('./GetById')
let GetByText = require('./GetByText')
let PostAPI = require('./Post')
module.exports = [

    {
        method: 'GET',
        path: '/coinWallet',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'coinWallet'],
            description: 'This API is used to Get User All coinWallet.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },

    {
        method: 'GET',
        path: '/filterCoinWallet',
        handler: GetByIdText.APIHandler,
        config: {
            tags: ['api', 'coinWallet'],
            description: 'This API is used to search coinWallet UserName Or Email.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByIdText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdText.response

        }
    },
    {
        method: 'GET',
        path: '/walletCustomer/{_id}',
        handler: GetById.APIHandler,
        config: {
            tags: ['api', 'coinWallet'],
            description: 'This API is used to get coinWallet by Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetById.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetById.response,

        }
    },
    {
        method: 'GET',
        path: '/coinWallet/{text}',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'coinWallet'],
            description: 'This API is used to search coinWallet By userName,City.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },
    {
        method: 'POST',
        path: '/coinWallet',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'coinWallet'],
            description: 'This API is used to post coinWallet.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response

        }
    },
];