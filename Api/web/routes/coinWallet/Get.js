'use strict'
const Joi = require("joi");
const logger = require('winston');
const coinwallet = require("../../../models/coinWallet");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {


    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    let condition = [
        {
            "$project": {
                coins: 1,
                coinValue: "$coins.Coin"
            }
        },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "userName": "$Data.firstName",
                "city": "$Data.address.city",
                "phone": "$Data.contactNumber",
                "coins": 1,
                "coinValue": 1

            }
        },
        { "$sort": { "_id": -1 } },
        { "$skip": offset * limit },
        { "$limit": limit }
    ];

    coinwallet.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                let coin = [];
                element.coinsCount = Object.keys(element.coins).length;

                for (var x in element.coins) {
                    var dt = {
                        name: x,
                        value: element.coins[x]
                    }
                    coin.push(dt);
                }
                element.coins = coin;



                if (element.phone !== null && element.phone !== undefined && element.phone !== '') {
                    var maskid = "";
                    var myemailId = element.phone;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.phone = maskid;
                }

            });

            coinwallet.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetcoinWallet')['200'], data: result, totalCount: ress }).code(200);
            });
        } else {
            return res({ message: req.i18n.__('GetcoinWallet')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinWallet']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetcoinWallet']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, queryValidator }