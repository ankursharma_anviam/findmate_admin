"use strict";
const Joi = require("joi");
const logger = require('winston');
const coinWallet = require("../../../models/coinWallet");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().description('search coinWallet By userName,City'),
    // offset: Joi.number().description("offset"),
    // limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var user = req.params.text.trim();
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;


    let condition = [

        {
            "$project": {
                coins: 1,
                coinValue: "$coins.Coin"

            }
        },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "userName": "$Data.firstName",
                "city": "$Data.address.city",
                "phone": "$Data.contactNumber",
                "coinValue": 1,
                "coins": 1


            }
        },
        {
            "$match": {
                "$or": [
                    { "userName": new RegExp(user, 'i') },
                    { "city": new RegExp(user, 'i') },


                ]
            }
        },
        // { "$skip": offset },
        // { "$limit": limit },
    ]
    console.log("======for search>>>", condition);



    coinWallet.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                let coin = [];
                element.coinsCount = Object.keys(element.coins).length;
                for (var x in element.coins) {
                    var dt = {
                        name: x,
                        value: element.coins[x]
                    }
                    coin.push(dt);
                }
                element.coins = coin;



                if (element.phone !== null && element.phone !== undefined && element.phone !== '') {
                    var maskid = "";
                    var myemailId = element.phone;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.phone = maskid;
                }
            });
            return res({ message: req.i18n.__('GetByIdTxt')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetByIdTxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetByIdTxt']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetByIdTxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }