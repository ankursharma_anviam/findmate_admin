"use strict";
const Joi = require("joi");
const logger = require('winston');
const subscription = require("../../../models/subscription");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().required().description('enter You want to search by subscriptionUserName or planName '),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 10;
    const offset = parseInt(req.query.offset) || 0;
    var user = req.params.text.trim();


    let condition = [

        {
            "$project": {
                planId: 1,
                purchaseDate: 1,
                purchaseTime: 1,
                planName: 1,
                userId: 1,
                status: 1,
                paymentGatewayTxnId: 1,
                paymentGateway: 1,
                subscriptionUserName: 1

            }
        },
        { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                subscriptionUserName: "$Data.firstName",
                planId: 1,
                purchaseDate: 1,
                purchaseTime: 1,
                planName: 1,
                userId: 1,
                status: 1,
                paymentGatewayTxnId: 1,
                paymentGateway: 1,
            }
        }

    ]
    if (req.query.text != "") {
        condition.push({
            "$match": {
                "$or": [
                    { "subscriptionUserName": new RegExp(req.query.text, 'i') },
                    { "planName": new RegExp(req.query.text, 'i') }
                ]
            }
        },
            //{ "$skip": offset * limit }, { "$limit": limit }
        );


    }
    console.log("======for search>>>", condition);


    subscription.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            subscription.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetWalletAppTxt')['200'], data: result, totalCount: ress }).code(200);
            });

        } else {
            return res({ message: req.i18n.__('GetPlanByText')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPlanByText']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPlanByText']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }