'use strict'
const Joi = require("joi");
const logger = require('winston');
const subscription = require("../../../models/subscription");
const local = require("../../../locales");

const queryValidator = Joi.object({
    planType: Joi.string().required().allow(["Active", "Expired"]).description('Configuration change by planType '),
    text: Joi.string().description('enter You want to search'),

    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {
    let condition = [];

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    switch (req.query.planType) {
        case "Active": {
            var code = 1;

            condition.push(
                { "$match": { "statusCode": { '$eq': 1 } } },
                {
                    "$project": {
                        //  subscriptionUserName: "$subscriptionUserName.firstName",
                        planId: 1,
                        purchaseDate: 1,
                        purchaseTime: 1,
                        planName: 1,
                        userId: 1,
                        status: 1,
                        paymentGatewayTxnId: 1,
                        paymentGateway: 1,
                        subscriptionUserName: 1,
                        cost: 1,
                        currencySymbol: 1

                    }
                },

                {
                    "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "subscriptionUserName" }
                }, { "$unwind": "$subscriptionUserName" },
                {
                    "$project": {
                        subscriptionUserName: "$subscriptionUserName.firstName",
                        planId: 1,
                        purchaseDate: 1,
                        purchaseTime: 1,
                        planName: 1,
                        userId: 1,
                        status: 1,
                        paymentGateway: 1,
                        paymentGatewayTxnId: 1,
                        cost: 1,
                        currencySymbol: 1

                    }
                },

                { "$sort": { "_id": -1 } }, { "$skip": offset * limit }, { "$limit": limit }
            )
            break;
        }
        case "Expired": {
            var code = 0;
            condition.push(

                { "$match": { "statusCode": { '$eq': 0 } } },

                {
                    "$project": {
                        // subscriptionUserName: "$subscriptionUserName.firstName",
                        planId: 1,
                        purchaseDate: 1,
                        purchaseTime: 1,
                        planName: 1,
                        userId: 1,
                        status: 1,
                        paymentGatewayTxnId: 1,
                        paymentGateway: 1,
                        subscriptionUserName: 1

                    }
                },

                {
                    "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "subscriptionUserName" }
                }, { "$unwind": "$subscriptionUserName" },
                {
                    "$project": {
                        subscriptionUserName: "$subscriptionUserName.firstName",
                        planId: 1,
                        purchaseDate: 1,
                        purchaseTime: 1,
                        planName: 1,
                        userId: 1,
                        status: 1,
                        paymentGateway: 1,
                        paymentGatewayTxnId: 1,

                    }
                },

                { "$sort": { "_id": -1 } }, { "$skip": offset * limit }, { "$limit": limit }
            )

            break;
        }


    }
    if (req.query.text != "") {
        condition.push({
            "$match": {
                "$or": [
                    { "subscriptionUserName": new RegExp(req.query.text, 'i') },
                    { "planName": new RegExp(req.query.text, 'i') }
                ]
            }
        },
            //{ "$skip": offset * limit }, { "$limit": limit }
        );


    }
    // else {
    //     condition.push({ "$skip": offset * limit }, { "$limit": limit });
    // }
    //condition.push({ "$skip": offset * limit }, { "$limit": limit })



    // console.log("-------->", condition)
    subscription.Aggregate(condition, (err, result) => {
        if (err) {

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            subscription.TotalCount({ statusCode: code }, (err, ress) => {
                return res({ message: req.i18n.__('Getsubscription')['200'], data: result, totalCount: ress }).code(200);
            });

            //return res({ message: req.i18n.__('Getsubscription')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('Getsubscription')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['Getsubscription']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['Getsubscription']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, queryValidator }