'use strict'
const Joi = require("joi");
const logger = require('winston');
const campaign = require("../../../models/campaign");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const payloadValidator = Joi.object({
    campaingId: Joi.string().required().description('Enter campaingId'),
    type: Joi.string().required().allow(["1", "2", "3"]).description('Configuration change sort '),

}).options({ allowUnknown: true });
/**
 * @method Get /campign
 * @param {*} req 
 * @param {*} res 
 */
const APIHandler = (req, res) => {
    let condition = [{ "$match": { "_id": ObjectID(req.query.campaingId) } },
    ];

    


    switch (req.query.type) {
        case "1": {
            condition.push(

                {
                    "$project": {
                        usersId: 1,
                        startDate: 1,
    
                    }
                },
    
                { "$unwind": "$usersId" },
                { "$lookup": { "from": "userList", "localField": "usersId", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
                        firstName: "$Data.firstName",
                        usersId: 1,
                        startDate: 1,
                        gender: "$Data.gender",
                        email: "$Data.email",
                        contactNumber: "$Data.contactNumber",
                        profilePic: "$Data.profilePic",
                        city: "$Data.address.city",
                        country: "$Data.address.country"
    
                    }
                },
                { "$sort": { "_id": -1 } },)
            break;
        }
        case "2": {
            condition.push(

                {
                    "$project": {
                        views: 1,
    
                    }
                },
    
                { "$unwind": "$views" },
                { "$lookup": { "from": "userList", "localField": "views.userId", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
    
                        firstName: "$Data.firstName",
                        startDate: 1,
                        gender: "$Data.gender",
                        email: "$Data.email",
                        contactNumber: "$Data.contactNumber",
                        profilePic: "$Data.profilePic",
                        viewCreation: "$views.creation",
                        userId: "$views.userId",
                        city: "$Data.address.city",
                        country: "$Data.address.country"
                    }
                }, { "$sort": { "_id": -1 } },
            )
    
            break;
        }
        case "3": {
            condition.push(
                {
                    "$project": {
                        clicked: 1,
    
                    }
                },
                { "$unwind": "$clicked" },
                { "$lookup": { "from": "userList", "localField": "clicked.userId", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
                        firstName: "$Data.firstName",
                        //clicked: 1,
                        startDate: 1,
                        gender: "$Data.gender",
                        email: "$Data.email",
                        contactNumber: "$Data.contactNumber",
                        profilePic: "$Data.profilePic",
                        clickedCreation: "$clicked.creation",
                        userId: "$clicked.userId",
                        city: "$Data.address.city",
                        country: "$Data.address.country"
                    }
                },
                { "$sort": { "_id": -1 } },
            )
            break;
        }
    }




    console.log("-----------------===============>>>>>>>>>>>>>>>>>>", JSON.stringify(condition));
    campaign.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                if (element.email !== null && element.email !== undefined && element.email !== '') {
                    var maskid = "";
                    var myemailId = element.email;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.email = maskid;
                }
                if (element.contactNumber !== null && element.contactNumber !== undefined && element.contactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.contactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.contactNumber = maskid;
                }
    
            });
            return res({ message: req.i18n.__('GetCampingById')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetCampingById')['204'], data: [] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetCampingById']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetCampingById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }