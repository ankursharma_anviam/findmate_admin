'use strict';

const Joi = require("joi");
const logger = require('winston');
const campainDetail = require("../../../models/campaign");
const ObjectID = require('mongodb').ObjectID;
const local  = require("../../../locales");


const payloadValidator = Joi.object({
    campainId: Joi.string().required().description('enter campainId id'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {


    let data = {
        _id: ObjectID(req.params.campainId)
    };

    campainDetail.Delete(data, (err, result) => {
        if (err) {
            return res({ message:  req.i18n.__('genericErrMsg')['500']}).code(500);
        } else {
            return res({  message:  req.i18n.__('DeleteCampaing')['200'] }).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['DeleteCampaing']['200']),
        },
        422: { message: Joi.any().default(local['DeleteCampaing']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { APIHandler, payloadValidator,response }