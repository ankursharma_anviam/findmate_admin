
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./GetUser');
let PostAPI = require('./Post');
let DeleteAPI = require('./Delete');
let GetCampaignAPI = require('./GetAll');
let GetByIdAPI = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/campaignUsers',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'campaign'],
            description: 'This API is use to get selected longitude  or latitude  vise user data.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'POST',
        path: '/campaingDetail',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'campaign'],
            description: 'This API is used to Post Details Of Camping.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'DELETE',
        path: '/campaing/{campainId}',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'campaign'],
            description: 'This API is used to Delete Id wise campaign.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    },
    {
        method: 'GET',
        path: '/campaingDetails',
        handler: GetCampaignAPI.APIHandler,
        config: {
            tags: ['api', 'campaign'],
            description: 'This API is use to get campain details.',
            auth: "adminJwt",
            validate: {
                query: GetCampaignAPI.queryValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetCampaignAPI.response

        }
    },
    {
        method: 'GET',
        path: '/campaignUsersById',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'campaign'],
            description: 'This API is use to  get id vise Target User record details.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response

        }
    }

   
];