let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let DeleteAPI = require('./Delete');
let PutAPI = require('./Put');

module.exports = [


    {
        method: 'POST',
        path: '/friendShip',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'friendShip'],
            description: 'This API is used to update friendShip.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/friendShip',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'friendShip'],
            description: 'This API is used to get friendShip.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'DELETE',
        path: '/friendShip',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'friendShip'],
            description: 'This API is used to Delete friendShip.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/friendShip',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'friendShip'],
            description: 'This API is used to update friendShip.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    }


];