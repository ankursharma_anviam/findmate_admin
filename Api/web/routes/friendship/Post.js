'use strict';
const local = require("../../../locales");
const Joi = require("joi");
const logger = require('winston');
const coinConfig = require("../../../models/coinConfig");
const ObjectID = require('mongodb').ObjectID;


const APIHandler = (req, res) => {


    var title = req.payload.title
    var coinTitle = req.payload.coinTitle
    var amount = parseInt(req.payload.amount)
    let ID = "5b15400364a7c370dbfa0660";
    let data = {
        [title]: {  
            [coinTitle]: amount
        }
    }
    // console.log("------------>", data)
    coinConfig.UpdateById(ID, data, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('postFriendship')['200'] }).code(200);
        }
    });
};

const payloadValidator = Joi.object({
    title: Joi.string().required().description('boostProfileForADay'),
    coinTitle: Joi.string().required().description("Gold"),
    amount: Joi.number().required().description("10000"),

}).options({ allowUnknown: true });


const response = {
    status: {
        200: {
            message: Joi.any().default(local['postFriendship']['200']),
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }