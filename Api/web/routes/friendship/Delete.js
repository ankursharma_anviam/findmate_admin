const Joi = require("joi");
const logger = require('winston');
const coinConfig = require("../../../models/coinConfig");
const local = require("../../../locales");



payloadValidator = Joi.object({
    field :Joi.string().required().description('field want to delete'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {

    let data = req.payload.field
    let ID = "5b15400364a7c370dbfa0660";


   coinConfig.UpdateDelete(ID,data, (err, result) => {
    if (err) {
        logger.silly(err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    } else {
        return res({ message: req.i18n.__('deleteFriendship')['200'] }).code(200);
    }
    });
};

// const response = {
//     status: {
//         200: {
//             message: Joi.any().default(local['PutcoinConfig']['200']),
//         },
//         204: { message: Joi.any().default(local['genericErrMsg']['204']) },
//         400: { message: Joi.any().default(local['genericErrMsg']['400']) },
//         500: { message: Joi.any().default(local['genericErrMsg']['500']) }
//     }
// }


module.exports = { APIHandler, payloadValidator }