const Joi = require("joi");
const logger = require('winston');
const friendship = require("../../../models/friendShip");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
    status: Joi.string().required().description('status'),

}).required();
const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;


    let condition = []

    condition = [
        // {
        //     //"$match": { "friendRequestStatus": "Accepted"  } },
        //     $match: { $or: [{ friendRequestStatus: 'Accepted' }, { friendRequestStatus: 'Pending' }] }
        // },
        {
            "$lookup": { "from": "userList", "localField": "friendRequestSentByerId", "foreignField": "_id", "as": "friendRequestSentBy" }
        }, { "$unwind": "$friendRequestSentBy" },


        {
            "$lookup": { "from": "userList", "localField": "friendRequestSentTo", "foreignField": "_id", "as": "friendRequestSentTo" }
        }, { "$unwind": "$friendRequestSentTo" },


        { "$sort": { "friendRequestSentOn": -1 } }
    ]
    if (req.query.status == 1) {
        var statusCode = { $or: [{ friendRequestStatus: 'Accepted' }, { friendRequestStatus: 'Pending' }] }
        condition.push(
            {
                //"$match": { "friendRequestStatus": "Accepted"  } },
                $match: { $or: [{ friendRequestStatus: 'Accepted' }, { friendRequestStatus: 'Pending' }] }
            }
        )
    }
    if (req.query.status == 2) {

        var statusCode = { "friendRequestStatus": "Deleted" }
        condition.push({
            $match: { "friendRequestStatus": "Deleted" }
        });
    }

    if (req.query.status == 3) {
        var statusCode = { "friendRequestStatus": "Unfriend" }
        condition.push({
            $match: { "friendRequestStatus": "Unfriend" }
        });
    }


    friendship.Aggregate(condition, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            friendship.TotalCount(statusCode, (err, ress) => {
                return res({ message: req.i18n.__('getFriendship')['200'], data: result, totalCount: ress }).code(200);
            });

            // return res({ message: req.i18n.__('getFriendship')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('getFriendship')['204'] }).code(204);
        }
    });
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['getFriendship']['200']), data: Joi.any(), totalCount: Joi.any(),
        },
        204: { message: Joi.any().default(local['getFriendship']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }