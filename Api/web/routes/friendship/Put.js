'use strict';
const local = require("../../../locales");
const Joi = require("joi");
const logger = require('winston');
const friendship = require("../../../models/friendShip");
const chatList = require("../../../models/chatList");
const ObjectID = require('mongodb').ObjectID;
const userList = require("../../../models/userList");


const APIHandler = (req, res) => {

    console.log("req.payloadreq.payloadreq.payload", req.payload)
    var status = req.payload.status
    var friendRequestSentTo = req.payload.id.friendRequestSentTo._id
    var friendRequestSentBy = req.payload.id.friendRequestSentBy._id

    let dataToUpdate1 = "members." + friendRequestSentTo;
    let dataToUpdate2 = "members." + friendRequestSentBy;
    let dataToUpdate3 = "members." + friendRequestSentTo + ".inactive";
    let dataToUpdate4 = "members." + friendRequestSentBy + ".inactive";
    console.log("friendRequestSentBy", friendRequestSentBy, "friendRequestSentTo", friendRequestSentTo)
    var data = {}
    var idOfChat = {
        [dataToUpdate1]: { "$exists": true },
        [dataToUpdate2]: { "$exists": true },
        isMatchedUser: 2

    }

    if (status == 1) {
        data = {
            "friendRequestStatus": "Unfriend",
            "friendUnfriendedOn": new Date().getTime(),
            "friendUnfriendedOnDate": new Date()
        }

        var chatToUpdate = {
            [dataToUpdate3]: true,
            [dataToUpdate4]: true,
            "UnfriendByAdminStatus": true
        }
    }
    if (status == 2) {
        data = {
            "friendRequestStatus": "Deleted",
            "friendDeletedOn": new Date().getTime(),
            "friendDeletedOnDate": new Date()
        }

        var chatToUpdate = {
            "deletedUser": "DeletedByAdmin",
            "deletedUserTimeStamp": new Date().getTime()
        }
    }
    var id = req.payload.id._id


    friendship.UpdateById(id, data, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            chatList.Update(idOfChat, chatToUpdate, (err, result) => {
                if (result) {
                    console.log("chat list also updated")

                    userList.UpdateByIdWithPull(friendRequestSentTo, friendRequestSentBy, (err, result) => {
                        if (result) {
                            console.log("chat list also updated")
                        }
                    })


                }
            })

            return res({ message: req.i18n.__('putFriendship')['200'] }).code(200);
        }
    });
};

const payloadValidator = Joi.object({
    // id: Joi.string().required().description('id'),
    // status: Joi.number().required().description("status"),

}).options({ allowUnknown: true });


const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['putFriendship']['200']),
    //     },
    //     204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}

module.exports = { APIHandler, response, payloadValidator }