'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/preferenceTable');
const local = require("../../../locales");


const payloadValidator = Joi.object({
    PreferenceTitle: Joi.string().required().description('PreferenceTitle'),
    TypeOfPreference: Joi.string().required().description('TypeOfPreference'),
    // Unit: Joi.string().description('Unit'),
    // optionsUnits: Joi.array().default().description('OptionsValue'),

}).options({ allowUnknown: true });
const APIHandler = (req, res) => {
    const unitObj = {
        "ftcm": ["ft", "cm"],
        "kmmi": ["km", "mi"],
        "ft": ["ft"],
        "cm": ["cm"],
        "km": ["km"],
        "mi": ["mi"],
        "year": ["year"],
        "kg": ["kg"],
        "pound": ["pound"],
    }

    for (const k in unitObj) {
        if (k == req.payload.optionsUnits) {
            var unit = unitObj[k]
        }

    }
    let condition = [
        {
            "$group": {
                "_id": null,
                "maxProirity": { "$max": "$Priority" }
            }
        }
    ]

    let data =
    {
        PreferenceTitle: req.payload.PreferenceTitle,
        TypeOfPreference: req.payload.TypeOfPreference,
        OptionsValue: req.payload.OptionsValue,
        optionsUnits: unit,
        ActiveStatus: 1,
    }
    if (data.optionsUnits == undefined) {

        delete data.optionsUnits
    }

    preferenceTable.Aggregate(condition, (err, result) => {

        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        req.payload["Priority"] = (result[0]["maxProirity"] + 1) || 1;
        data.Priority = req.payload.Priority
        console.log("===========>", data)
        preferenceTable.Insert(data, (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {

                return res({ message: req.i18n.__('PostPrefrances')['200'] }).code(200);
            }

        })
    })


};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostPrefrances']['200']),
        },
        204: { message: Joi.any().default(local['PostPrefrances']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }