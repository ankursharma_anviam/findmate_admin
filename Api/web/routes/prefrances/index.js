
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete")
module.exports = [
    {
        method: 'GET',
        path: '/preferences',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'prefrances'],
            description: 'This API is used to get an user prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'POST',
        path: '/preference',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'prefrances'],
            description: 'This API is used to post an prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },
    {
        method: 'Delete',
        path: '/preference/{prefId}',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'preference'],
            description: 'This API is used to delete preference.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    }
];