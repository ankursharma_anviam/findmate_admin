'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require("../../../models/preferenceTable");
const searchPreferencesCollection = require("../../../models/searchPreferences");
const local = require("../../../locales");
const Promise = require('promise');


const APIHandler = (req, res) => {

    let Mypreference = [];
    let searchPreferences = [];

    let getMyPreference = () => {
        return new Promise((resolve, reject) => {
            preferenceTable.Select({}, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                if (!result) {
                    return res({ message: req.i18n.__('GetPrefrances')['412'] }).code(412);
                } else {
                    return resolve(Mypreference.push(result));
                }

            });
        });
    }
    let getSearchPreferences = () => {
        return new Promise((resolve, reject) => {
            searchPreferencesCollection.Select({}, (err, res) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                if (!res) {
                    return res({ message: req.i18n.__('GetPrefrances')['412'] }).code(412);
                } else {
                    return resolve(searchPreferences.push(res));
                }

            });
        });
    }
    getMyPreference()
        .then(function () { return getSearchPreferences() })
        .then(function () { return res({ message: req.i18n.__('GetPrefrances')['200'], data: { Mypreference: Mypreference, searchPreferences: searchPreferences } }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPrefrances']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPrefrances']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, response }