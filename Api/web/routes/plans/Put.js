'use strict'
const Joi = require("joi");
const logger = require('winston');
const plans = require("../../../models/plans");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const payloadValidator = Joi.object({
    _id: Joi.string().required().description("_id,Eg. 435163843514354").error(new Error('_id is missing')),
    newcost: Joi.number().required().description("for EX. 100$").error(new Error('cost is missing')),
    newcurrencyCode: Joi.string().required().description("currencyCode,Eg. +91").error(new Error('currencyCode is missing')),
    newcurrencySymbol: Joi.string().required().description("currencySymbol,Eg. $").error(new Error('currencySymbol is missing')),
    newdurationInDays: Joi.number().required().description("Ex. 30Day").error(new Error('durationInDays  is missing')),
    newdurationInMonths: Joi.number().required().description("Ex. 30Day").error(new Error('durationInMonths  is missing')),
    newlikeCount: Joi.string().required().description("Enter likeCount ex. 200").error(new Error('likeCount is missing')),
    newplanName: Joi.string().required().description("planName ex. march dhamaka").error(new Error('planName is missing')),
    newactualId: Joi.string().required().description("actualId ex. 1235456").error(new Error('actualId is missing')),
    newrewindCount: Joi.string().required().description("rewindCount,Eg. 300").error(new Error('rewindCount is missing.')),
    newdescription: Joi.string().required().description("for E.x description").error(new Error('description is missing')),
    newwhoLikeMe: Joi.boolean().default("false").description('true/false'),
    newwhoSuperLikeMe: Joi.boolean().default("false").description('true/false'),
    newrecentVisitors: Joi.boolean().default("false").description('true/false'),
    newreadreceipt: Joi.boolean().default("false").description('true/false'),
    newpassport: Joi.boolean().default("false").description('true/false'),
    newnoAdds: Joi.boolean().default("false").description('true/false'),
    newhideDistance: Joi.boolean().default("false").description('true/false'),
    newhideAge: Joi.boolean().default("false").description('true/false'),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    

    //var _id = new ObjectID();
    var dataToSend = {

        "planName": req.payload.newplanName,
        "actualId": req.payload.newactualId,
        "description": req.payload.newdescription,
        "timestamp": new Date().getTime(),
        "currencyCode": req.payload.newcurrencyCode,
        "currencySymbol": req.payload.newcurrencySymbol,
        "cost": req.payload.newcost,
        "durationInDays": req.payload.newdurationInDays,
        "durationInMonths": req.payload.newdurationInMonths,
        "likeCount": (req.payload.newlikeCount == "unlimited") ? req.payload.newlikeCount : ((req.payload.newlikeCount) >= 0) ? (req.payload.newlikeCount) : 0,
        "rewindCount": (req.payload.newrewindCount == "unlimited") ? req.payload.newrewindCount : ((req.payload.newrewindCount) >= 0) ? (req.payload.newrewindCount) : 0,
        "whoLikeMe": req.payload.newwhoLikeMe,
        "whoSuperLikeMe": req.payload.newwhoSuperLikeMe,
        "recentVisitors": req.payload.newrecentVisitors,
        "readreceipt": req.payload.newreadreceipt,
        "passport": req.payload.newpassport,
        "noAdds": req.payload.newnoAdds,
        "hideDistance": req.payload.newhideDistance,
        "hideAge": req.payload.newhideAge,
        "statusCode": 1,
        "status": "active"

    }
console.log(dataToSend,req.payload._id)
plans.UpdateById(req.payload._id, dataToSend, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('EditPlan')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['EditPlan']['200']),
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }