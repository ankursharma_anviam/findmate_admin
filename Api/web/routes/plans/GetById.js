'use strict';

const Joi = require("joi");
const logger = require('winston');
const userList = require("../../../models/plans");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;


    try {
        let condition = [
            { "$match": { "_id": ObjectID(req.params._id) } },
            {
                "$project": {
                    cost: 1, currencyCode: 1, currencySymbol: 1,
                    durationInDays: 1, hideAge: 1,actualId:1,
                    hideDistance: 1, likeCount: 1,durationInMonths:1,
                    "noAdds": 1, "passport": 1, "planName": 1,
                    "readreceipt": 1, "recentVisitors": 1,
                    "rewindCount": 1,
                    "status": 1, "statusCode": 1, "superLikeCount": 1,
                    "whoLikeMe": 1,
                    "whoSuperLikeMe": 1,
                    "description": 1,
                    timestamp:1

                }
            },
            { "$skip": offset  }, { "$limit": limit }
        ];

        userList.Aggregate(condition, (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            }

            userList.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetPlanById')['200'], data: result, totalCount: ress }).code(200);
            });

            //return res({ message: req.i18n.__('GetPlanById')['200'], data: result }).code(200);
        });
    } catch (error) {

        return res({ message: req.i18n.__('GetPlanById')['204'] }).code(204);
    }


};

const payloadValidator = Joi.object({
    _id: Joi.string().required().description('_id'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPlanById']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPlanById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }