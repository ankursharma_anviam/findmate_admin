'use strict';

const Joi = require("joi");
const logger = require('winston');
const Cryptr = require('cryptr');
const cryptr = new Cryptr("3Embed1008");
const plans = require("../../../models/plans");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const payloadValidator = Joi.object({
    cost: Joi.number().required().description("for EX. 100$").error(new Error('cost is missing')),
    currencyCode: Joi.string().required().description("currencyCode,Eg. +91").error(new Error('currencyCode is missing')),
    currencySymbol: Joi.string().required().description("currencySymbol,Eg. $").error(new Error('currencySymbol is missing')),
    durationInDays: Joi.number().required().description("Ex. 30Day").error(new Error('durationInDays  is missing')),
    durationInMonths: Joi.number().required().description("Ex. 30Day").error(new Error('durationInMonths  is missing')),
    likeCount: Joi.string().required().description("Enter likeCount ex. 200").error(new Error('likeCount is missing')),
    planName: Joi.string().required().description("planName ex. march dhamaka").error(new Error('planName is missing')),
    actualId: Joi.string().required().description("actualId ex. 1235456").error(new Error('actualId is missing')),
    rewindCount: Joi.string().required().description("rewindCount,Eg. 300").error(new Error('rewindCount is missing.')),
    description: Joi.string().required().description("for E.x description").error(new Error('description is missing')),
    whoLikeMe: Joi.boolean().default("false").description('true/false'),
    whoSuperLikeMe: Joi.boolean().default("false").description('true/false'),
    recentVisitors: Joi.boolean().default("false").description('true/false'),
    readreceipt: Joi.boolean().default("false").description('true/false'),
    passport: Joi.boolean().default("false").description('true/false'),
    noAdds: Joi.boolean().default("false").description('true/false'),
    hideDistance: Joi.boolean().default("false").description('true/false'),
    hideAge: Joi.boolean().default("false").description('true/false'),



}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let data = {
        "planName": req.payload.planName,
        "actualId": req.payload.actualId,
        "description": req.payload.description,
        "timestamp": new Date().getTime(),
        "currencyCode": req.payload.currencyCode,
        "currencySymbol": req.payload.currencySymbol,
        "cost": req.payload.cost,
        "durationInDays": req.payload.durationInDays,
        "durationInMonths": req.payload.durationInMonths,
        "likeCount": (req.payload.likeCount == "unlimited") ? req.payload.likeCount : ((req.payload.likeCount) >= 0) ? (req.payload.likeCount) : 0,
        "rewindCount": (req.payload.rewindCount == "unlimited") ? req.payload.rewindCount : ((req.payload.rewindCount) >= 0) ? (req.payload.rewindCount) : 0,
        "whoLikeMe": req.payload.whoLikeMe,
        "whoSuperLikeMe": req.payload.whoSuperLikeMe,
        "recentVisitors": req.payload.recentVisitors,
        "readreceipt": req.payload.readreceipt,
        "passport": req.payload.passport,
        "noAdds": req.payload.noAdds,
        "hideDistance": req.payload.hideDistance,
        "hideAge": req.payload.hideAge,
        "statusCode": 1,
        "status": "active"
    }

    plans.Insert(data, (err, result) => {
        if (err) {

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PostPlans')['200'] }).code(200);
        }
    });

};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostPlans']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }