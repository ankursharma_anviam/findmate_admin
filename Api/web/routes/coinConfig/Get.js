const Joi = require("joi");
const logger = require('winston');
const coinConfig = require("../../../models/coinConfig");
const local = require("../../../locales");

const APIHandler = (req, res) => {

  
    coinConfig.Select({},{_id:0}, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {

            return res({ message: req.i18n.__('GetCoinConfig')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetCoinConfig')['204'] }).code(204);
        }
    });
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetCoinConfig']['200']), data: Joi.any(),
        },
        204: { message: Joi.any().default(local['GetCoinConfig']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }