const logger = require('winston');
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const sendMail = require('../../library/mailgun');

ipc.config.id = 'rabbitmqworker';
ipc.config.retry = 1000;
ipc.config.silent = true;

ipc.connectTo('rabbitmqserver', () => {
    ipc.of.rabbitmqserver.on('connect', () => {
        ipc.of.rabbitmqserver.emit('consumer.connected', {});
        logger.info("RabbitMQ consumer connected");
    });
});


logger.info('step 4: child Process Started.......' + process.pid + " ");

rabbitMq.connect(() => {

    prepareConsumer(rabbitMq.getChannelEmail(), rabbitMq.queueEmail, rabbitMq.get());
});



function prepareConsumer(channelEmail, queueEmail, amqpConn) {

    channelEmail.assertQueue(queueEmail, { durable: false }
        , function (err, queue) {
            logger.info("RabbitMQ Message count", queue.messageCount + " " + queue.consumerCount);
            if (queue.messageCount > 0) {
                channelEmail.consume(queueEmail, function (msg) {

                    logger.info("consume started" + new Date());

                    logger.info("msg  :: ", msg.content.toString());
                    console.log('Hello Email from Worker ===>>> '+ process.pid);
                    var data = JSON.parse(msg.content.toString());
                    let param = {
                        from: data.from,
                        to: data.to,
                        subject: data.subject,
                        html: data.html,
                        text: data.html
                    }
                    sendMail.sendMail(param, (err, res) => { });

                    // ipc.of.rabbitmqserver.emit('message.consumed', { "data": "done : 1" });
                    //   console.log("consume end"+new Date());
                }, { noAck: true }, function (err, ok) {
                    //To check if need to exit worker
                });
            }
        });
}