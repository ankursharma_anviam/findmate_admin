'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    MONGODB_URL_azar: joi.string()
        .required()
}).unknown()
    .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}

const config = {
    mongodb: {
        url: envVars.MONGODB_URL_azar
    }
}

module.exports = config